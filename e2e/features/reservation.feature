Feature: Reservation
  In order to use equipment
  As a user
  I want to be able to make reservation

  Scenario: create reservation from dashboard page
    Given resolution 1024x640
    And login page
    When I fill "login" field with "admin"
    And I fill "password" field with "qwerty12"
    And I press Enter
    Then I see dashboard page
    And I click add reservation button
    Then I see reservation form
    And I fill in reservation form
    And I click save button
    Then I see my reservation on reservations list
