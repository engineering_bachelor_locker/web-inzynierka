Feature: Login
  In order to access application
  As a user
  I want to be able to log in to a system

  Scenario: successfuly by pressing Enter
    Given resolution 1024x640
    And login page
    When I fill "login" field with "admin"
    And I fill "password" field with "qwerty12"
    And I press Enter
    Then I see dashboard page

  Scenario: successfuly by clicking Login
    Given resolution 1024x640
    And login page
    When I fill "login" field with "adilab"
    And I fill "password" field with "qwerty12"
    And I click Login button
    Then I see dashboard page

  Scenario: reject when wrong credencials
    Given resolution 1024x640
    And login page
    When I fill "login" field with "sureaninvalidlogin"
    And I fill "password" field with "incorrectpassword"
    And I click Login button
    Then I see text "Wystąpił błąd"
