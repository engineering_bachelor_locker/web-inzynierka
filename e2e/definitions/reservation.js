const I = actor();

When('I click add reservation button', async () => {
  await I.click('#add-reservation-button');
});

When('I see reservation form', async () => {
  I.wait(2);
  await I.waitForText('Rezerwacja', 20);
});

When('I fill in reservation form', async () => {
  //set reservation start
  I.click('#reservation_starts_at');
  I.click({
    xpath:
      '/html/body/div[4]/div[2]/div/div[1]/div[2]/div/div[2]/div/div[5]/div[2]/button/span[1]'
  });
  I.click({ xpath: '/html/body/div[4]/div[2]/div/div[2]/button[2]/span[1]' });
  //set reservation end
  I.click('#reservation_ends_at');
  I.click({
    xpath:
      '/html/body/div[4]/div[2]/div/div[1]/div[2]/div/div[2]/div/div[5]/div[4]/button/span[1]'
  });
  I.click({ xpath: '/html/body/div[4]/div[2]/div/div[2]/button[2]/span[1]' });
  //set equipment
  I.click({
    xpath:
      '//*[@id="app"]/div/section/div[2]/div/div[1]/div/div[1]/div/div[2]/div/div/div/label'
  });
  I.click({ xpath: '//*[@id="menu-equipment_id"]/div[2]/ul/li[3]' });
  //set description
  await I.fillField('description', 'Test description');
});

When('I click save button', async () => {
  await I.click('#reservation_save_button');
});

When('I see my reservation on reservations list', async () => {
  await I.wait(3);
  await I.waitForText('Mikroskop', 20);
});
