const I = actor();

When('I press Enter', async () => {
  await I.pressKey('Enter');
});

Given('resolution {int}x{int}', async (height, width) => {
  await I.resizeWindow(height, width);
});

When('I go to link {string}', link => {
  I.goToLink(link);
});

Then('I see text {string}', async text => {
  I.wait(3);
  await I.waitForText(text, 20);
});

Then('I fill {string} field with {string}', async (name, value) => {
  await I.clearField(name);
  await I.fillField(name, value);
});

Then('I click save', async () => {
  await I.wait(1);
  await I.click('Zapisz');
});

Then("I don't see text {string}", async text => {
  await I.dontSee(text);
});

When('I log out', async () => {
  await I.waitForVisible(menu.menu_icon, 20);
  await I.click(menu.menu_icon);
  await I.waitForVisible(menu.logout, 20);
  await I.click(menu.logout);
});

Then('I see dashboard page', async () => {
  I.wait(3);
  await I.waitForText('Moje rezerwacje', 20);
  await I.seeInCurrentUrl('/dashboard/reservations');
});
