const I = actor();

Given('login page', async () => {
  I.amOnPage('/');
  await I.waitForText('Panel logowania', 20);
});

When('I go to login page', () => {
  I.amOnPage('/');
});

When('I log in with {string} and {string}', (email, password) => {
  I.login(email, password);
  I.amOnPage('/');
});

When('I click Login button', async () => {
  await I.click('Zaloguj');
});

When('I click email field', async () => {
  await I.click('#login_panel_login');
});

Then('I am on login page', async () => {
  I.wait(2);
  await I.waitForText('Panel logowania', 20);
});
