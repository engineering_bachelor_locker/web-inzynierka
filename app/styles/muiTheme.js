import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  direction: 'rtl',
  palette: {
    primary: {
      main: '#3d5afe'
    },
    secondary: {
      light: '#7788AA',
      main: '#ffc400',
      contrastText: '#ffc400'
    }
  },
  overrides: {
    DateTimePickerHeader: {
      hourMinuteLabel: {
        flexDirection: 'row'
      }
    }
  }
});

export default theme;
