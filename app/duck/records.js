import { Record } from 'immutable';
import { api_states } from './consts';

export const Notifications = Record({
  list: []
});

export const Notification = Record({
  notification: null,
  timer: null,
  uuid: null
});

export const User = Record({
  id: null,
  email: '',
  first_name: '',
  last_name: '',
  role: ''
});

export const Rfid = Record({
  code: null,
  has_active_rfid: false
});

export const AppSettings = Record({
  userProfileFetched: false,
  user: new User(),
  login_state: api_states.DONE,
  rfid_state: api_states.DONE,
  rfid_code_state: api_states.DONE,
  rfid: new Rfid()
});
