import types from './types';
import { uuid, message_types, api_states } from './consts';
import { Notification } from './records';
import { normalizeProfileInformation, normalizeRfidData } from './normalizers';
import history from '../utils/history';
import RestClient from '../services/RestClient';
import { AUTH_KEY } from '../utils/consts';
import { setCookie, getCookie, destroyCookie } from '../utils/cookieHandler';
import { appUrls } from '../services/appUrls';
import { getUserId } from './selectors';

export const redirect = url => () => history.push(url);

export const addNotification = (message, timeout) => dispatch => {
  const notification_uuid = uuid();
  const timer = setTimeout(
    () => {
      dispatch(removeNotification(notification_uuid));
    },
    !timeout ? message.type.timeout : timeout
  );

  const notification = new Notification({
    uuid: notification_uuid,
    notification: message,
    timer: timer
  });

  dispatch({
    type: types.ADD_NOTIFICATION,
    notification
  });
};

export const removeNotification = (uuid = 0) => dispatch => {
  dispatch({
    type: types.REMOVE_NOTIFICATION,
    uuid
  });
};

const loginStateChanged = state => ({
  type: types.USER_LOGIN_STATE_CHANGED,
  state
});

export const signIn = (login, password) => dispatch => {
  dispatch(loginStateChanged(api_states.IN_PROGRESS));
  return RestClient.signIn(login, password).then(
    response => {
      dispatch(loginStateChanged(api_states.DONE));
      const token = response.data.token;
      setCookie(AUTH_KEY, token);
      window.location = appUrls.DASHBOARD;
    },
    () => {
      dispatch(loginStateChanged(api_states.DONE));
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};

export const signOut = () => {
  RestClient.signOut()
    .then(() => {
      destroyCookie(AUTH_KEY);
      window.location = appUrls.ROOT;
    })
    .catch(() => {
      destroyCookie(AUTH_KEY);
      window.location = appUrls.ROOT;
    });
};

export const getToken = () => getCookie(AUTH_KEY);

export const fetchProfileInformation = () => dispatch => {
  return RestClient.getProfileInformation().then(
    response => {
      const user = normalizeProfileInformation(response);
      dispatch({
        type: types.USER_PROFILE_FETCHED,
        user
      });
    },
    () => {
      //TODO handle this
    }
  );
};

const rfidStateChanged = state => ({
  type: types.RFID_STATE_CHANGED,
  state
});

export const fetchRfidAssigmentCode = () => dispatch => {
  dispatch(rfidStateChanged(api_states.IN_PROGRESS));
  return RestClient.getRfidAssigmentCode().then(
    response => {
      dispatch(rfidStateChanged(api_states.DONE));
      const rfid = normalizeRfidData(response);
      dispatch({
        type: types.RFID_DATA_FETCHED,
        rfid
      });
    },
    () => {
      dispatch(rfidStateChanged(api_states.DONE));
    }
  );
};

const rfidCodeStateChanged = state => ({
  type: types.RFID_CODE_STATE_CHANGED,
  state
});

export const generateRfidAssignmentCode = () => dispatch => {
  dispatch(rfidCodeStateChanged(api_states.IN_PROGRESS));
  return RestClient.generateRfidAssigmentCode().then(
    response => {
      dispatch(rfidCodeStateChanged(api_states.DONE));
      dispatch({
        type: types.RFID_CODE_GENERATED,
        code: response.data.code
      });
    },
    () => {
      dispatch(rfidCodeStateChanged(api_states.DONE));
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};

export const blockRfidCardAndGenerateCode = () => (dispatch, getState) => {
  const userId = getUserId(getState());
  dispatch(rfidCodeStateChanged(api_states.IN_PROGRESS));
  return RestClient.blockCardAndGenerateRfidAssigmentCode(userId).then(
    response => {
      dispatch(rfidCodeStateChanged(api_states.DONE));
      dispatch({
        type: types.RFID_CODE_GENERATED,
        code: response.data.code
      });
    },
    () => {
      dispatch(rfidCodeStateChanged(api_states.DONE));
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};
