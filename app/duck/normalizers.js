import { User, Rfid } from './records';

export const normalizeProfileInformation = response =>
  new User({
    ...response.data
  });

export const normalizeRfidData = response =>
  new Rfid({
    ...response.data
  });
