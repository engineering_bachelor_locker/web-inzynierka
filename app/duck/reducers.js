import types from './types';
import { Notifications, AppSettings } from './records';

export const appSettings = (state = new AppSettings(), action) => {
  switch (action.type) {
    case types.USER_PROFILE_FETCHED:
      return state.set('userProfileFetched', true).set('user', action.user);
    case types.USER_LOGIN_STATE_CHANGED:
      return state.set('login_state', action.state);
    case types.RFID_STATE_CHANGED:
      return state.set('rfid_state', action.state);
    case types.RFID_DATA_FETCHED:
      return state.set('rfid', action.rfid);
    case types.RFID_CODE_STATE_CHANGED:
      return state.set('rfid_code_state', action.state);
    case types.RFID_CODE_GENERATED:
      return state.setIn(['rfid', 'code'], action.code);
  }

  return state;
};

export const notifications = (state = new Notifications(), action) => {
  switch (action.type) {
    case types.ADD_NOTIFICATION:
      return state.update('list', list => list.concat([action.notification]));
    case types.REMOVE_NOTIFICATION:
      return state.update('list', list => {
        return list.filter(el => {
          if (el.uuid === action.uuid) {
            clearTimeout(el.timer);
            return false;
          }
          return true;
        });
      });
  }

  return state;
};

export default global;
