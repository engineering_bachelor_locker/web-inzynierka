import { USER_ROLES } from '../utils/consts';
import { api_states } from './consts';

export const isUserProfileDataFetched = state =>
  state.appSettings.userProfileFetched;

export const getUserRole = state => state.appSettings.user.role;

export const getUserEmail = state => state.appSettings.user.email;

export const getUserId = state => state.appSettings.user.id;

export const getNotifications = state => state.notifications.list;

export const isUserLoggedIn = state => !!state.appSettings.user.role;

export const isAdmin = state => getUserRole(state) === USER_ROLES.ADMIN;

export const isUserLogging = state =>
  state.appSettings.login_state === api_states.IN_PROGRESS;

export const isRfidLoading = state =>
  state.appSettings.rfid_state === api_states.IN_PROGRESS;

export const isRfidCodeLoading = state =>
  state.appSettings.rfid_code_state === api_states.IN_PROGRESS;

export const getRfidAssignmentCode = state => state.appSettings.rfid.code;

export const hasUserActiveRfidCard = state =>
  state.appSettings.rfid.has_active_rfid;
