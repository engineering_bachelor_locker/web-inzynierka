export const message_types = {
  success: { icon: 'check_circle', color: '#76CC66', timeout: 5000 },
  error: { icon: 'remove_circle', color: '#dd3300', timeout: 5000 },
  warning: { icon: 'warning', color: '#F0650A', timeout: 5000 }
};

export const uuid = () =>
  'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    let r = (Math.random() * 16) | 0,
      v = c == 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });

export const api_states = {
  IN_PROGRESS: 'in-progress',
  DONE: 'done'
};

export const NEW_FORM_ID = 'new';

export const room_states = {
  ACTIVE: 'active',
  INACTIVE: 'inactive',
  DELETED: 'deleted'
};

export const getRoomStateName = state => `rooms.states.${state}`;

export const equipment_states = {
  ACTIVE: 'active',
  INACTIVE: 'inactive',
  DELETED: 'deleted'
};

export const getEquipmentStateName = state => `equipment.states.${state}`;

export const reservation_states = {
  ACTIVE: 'active',
  INACTIVE: 'inactive',
  DELETED: 'deleted'
};

export const getReservationStateName = state => `reservations.states.${state}`;

export const USER_ROLES = {
  ADMIN: 'admin',
  CASUAL: 'casual'
};
