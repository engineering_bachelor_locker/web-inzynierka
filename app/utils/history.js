import createHistory from 'history/createBrowserHistory';

export default createHistory();

export const mergedQueryStringWithParams = (queryString, newParams) => {
  const queryObject = queryStringToObject(queryString);
  const newQueryObject = {
    ...queryObject,
    ...newParams
  };
  return objectToQueryString(newQueryObject);
};

export const objectToQueryString = object => {
  const pairs = Object.keys(object).map(key => `${key}=${object[key]}`);

  return `?${pairs.join('&')}`;
};

export const queryStringToObject = queryString => {
  const keyValuePairs = queryString.replace('?', '').split('&');

  return keyValuePairs.reduce((queryObject, current) => {
    if (current.length > 0) {
      const keyValue = current.split('=');
      queryObject[keyValue[0]] = keyValue[1];
    }

    return queryObject;
  }, {});
};
