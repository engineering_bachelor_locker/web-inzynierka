const USER_PERMISSIONS = {};

export const hasAccessToPath = (role, path) =>
  USER_PERMISSIONS[role] ? USER_PERMISSIONS[role].paths[path] : false;
