export const USER_ROLES = {
  ADMIN: 'admin'
};

export const USER_ROLES_TRANSLATIONS = {
  [USER_ROLES.ADMIN]: 'roles.admin'
};

export const AUTH_KEY = 'token';
