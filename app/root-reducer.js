import { combineReducers } from 'redux';

import { notifications, appSettings } from './duck/reducers';
import { rooms } from './containers/Rooms/duck/reducer';
import { equipments } from './containers/Equipment/duck/reducer';
import { history } from './containers/History/duck/reducer';
import { calendar } from './containers/Calendar/duck/reducer';
import {
  reservations,
  reservation_equipment_list
} from './containers/Reservations/duck/reducer';

export default combineReducers({
  appSettings,
  notifications,
  rooms,
  equipments,
  reservations,
  reservation_equipment_list,
  history,
  calendar
});
