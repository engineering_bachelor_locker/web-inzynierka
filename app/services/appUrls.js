export const appUrls = {
  ROOT: '/',
  DASHBOARD: '/dashboard',
  RESERVATIONS: '/dashboard/reservations',
  RESERVATION: '/dashboard/reservations/:id',
  CALENDAR: '/dashboard/calendar',
  EQUIPMENTS: '/dashboard/equipment',
  EQUIPMENT: '/dashboard/equipments/:id',
  USERS: '/dashboard/users',
  USER: '/dashboard/user:id',
  ROOMS: '/dashboard/rooms',
  ROOM: '/dashboard/rooms/:id',
  ROOM_VIEW: '/dashboard/rooms/:id/view',
  ROOM_EQUIPMENT: '/dashboard/rooms/:room_id/equipment/:id',
  HISTORY_LIST: '/dashboard/history',
  HISTORY_ENTRY: '/dashboard/history/:id',
  RFID: '/dashboard/rfid'
};
