import axios from 'axios';
import { apiUrls } from './apiUrls';
import '../utils/format';
import { getToken } from '../duck/actions'; //signOut

export default class RestClient {
  static signIn(login, password) {
    const options = {
      method: 'POST',
      data: { login, password }
    };
    return this.makeRequest(apiUrls.SIGN_IN, options);
  }

  static signOut() {
    return this.makeSecureRequest(apiUrls.SIGN_OUT, { method: 'DELETE' });
  }

  static getRfidAssigmentCode() {
    return this.makeSecureRequest(apiUrls.RFID_ASSIGMENT, {
      method: 'GET'
    });
  }

  static generateRfidAssigmentCode() {
    return this.makeSecureRequest(apiUrls.RFID_ASSIGMENT, {
      method: 'POST'
    });
  }

  static blockCardAndGenerateRfidAssigmentCode(user_id) {
    return this.makeSecureRequest(apiUrls.BLOCK_RFID_CARD.format({ user_id }), {
      method: 'POST'
    });
  }

  static getRooms() {
    return this.makeSecureRequest(apiUrls.ROOMS, { method: 'GET' });
  }

  static getProfileInformation() {
    return this.makeSecureRequest(apiUrls.ME, { method: 'GET' });
  }

  static getRoom(id) {
    return this.makeSecureRequest(apiUrls.ROOM.format({ room_id: id }), {
      method: 'GET'
    });
  }

  static getReservation(id) {
    return this.makeSecureRequest(apiUrls.RESERVATION.format({ id }), {
      method: 'GET'
    });
  }

  static getReservations(params = {}) {
    const limit = params.limit || 500;
    return this.makeSecureRequest(apiUrls.RESERVATIONS, {
      method: 'GET',
      params: {
        ...params,
        limit
      }
    });
  }

  static removeRoom(id) {
    return this.makeSecureRequest(apiUrls.ROOM.format({ room_id: id }), {
      method: 'DELETE'
    });
  }

  static saveRoom(data) {
    const roomId = data.id;
    const url = roomId
      ? apiUrls.ROOM.format({ room_id: roomId })
      : apiUrls.ROOMS;
    const options = {
      method: roomId ? 'PATCH' : 'POST',
      data
    };

    return this.makeSecureRequest(url, options);
  }

  static removeReservation(id) {
    return this.makeSecureRequest(apiUrls.RESERVATION.format({ id }), {
      method: 'DELETE'
    });
  }

  static saveReservation(data) {
    const id = data.id;
    const url = id ? apiUrls.RESERVATION.format({ id }) : apiUrls.RESERVATIONS;
    const options = {
      method: id ? 'PATCH' : 'POST',
      data
    };

    return this.makeSecureRequest(url, options);
  }

  static saveEquipment(data, room_id) {
    const roomId = room_id || data.room_id;
    const equipmentId = data.id;
    const url = equipmentId
      ? apiUrls.ROOM_EQUIPMENT.format({ room_id: roomId, id: equipmentId })
      : apiUrls.ROOM_EQUIPMENTS.format({ room_id: roomId });
    const options = {
      method: equipmentId ? 'PATCH' : 'POST',
      data
    };

    return this.makeSecureRequest(url, options);
  }

  static removeEquipment(room_id, id) {
    return this.makeSecureRequest(
      apiUrls.ROOM_EQUIPMENT.format({ room_id, id }),
      {
        method: 'DELETE'
      }
    );
  }

  static getEquipments() {
    return this.makeSecureRequest(apiUrls.EQUIPMENT, { method: 'GET' });
  }

  static getHistory() {
    return this.makeSecureRequest(apiUrls.HISTORY, { method: 'GET' });
  }

  static getEquipment(room_id, id) {
    return this.makeSecureRequest(
      apiUrls.ROOM_EQUIPMENT.format({ room_id, id }),
      {
        method: 'GET'
      }
    );
  }

  static makeSecureRequest(subPath, options = {}) {
    options.headers = {
      ...options.headers,
      Authorization: getToken()
    };
    return this.makeRequest(subPath, options);
  }

  static makeRequest(subPath, options = {}) {
    const headers = options.headers || {};
    headers['Content-Type'] = 'application/json';
    headers['Accept'] = 'application/json';
    options.headers = headers;
    options.url = `${apiUrls.API_URL}/${subPath.replace(/^\//, '')}`;

    return axios(options);
  }
}

// axios.interceptors.response.use(
//   response => {
//     return response;
//   },
//   error => {
//     if (error.response && error.response.status == 401) {
//       signOut();
//     }
//     throw error;
//   }
// );
