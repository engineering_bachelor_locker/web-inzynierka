export const apiUrls = {
  API_URL: API_URL,
  ME: '/api/v1/users/me',
  SIGN_IN: '/api/v1/sessions/base',
  SIGN_OUT: '/api/v1/sessions',
  ROOMS: '/api/v1/rooms?order=number:asc',
  ROOM: '/api/v1/rooms/:room_id',
  ROOM_EQUIPMENTS: '/api/v1/rooms/:room_id/equipment',
  ROOM_EQUIPMENT: '/api/v1/rooms/:room_id/equipment/:id',
  EQUIPMENT: '/api/v1/equipment?order=number:asc',
  RESERVATIONS: '/api/v1/reservations',
  RESERVATION: '/api/v1/reservations/:id',
  HISTORY: '/api/v1/access_histories?order=created_at:desc',
  RFID_ASSIGMENT: '/api/v1/rfid_assignments',
  BLOCK_RFID_CARD: '/api/v1/users/:user_id/rfids/block'
};
