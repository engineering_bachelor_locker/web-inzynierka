import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { I18nextProvider } from 'react-i18next';
import { Router, Switch, Route } from 'react-router-dom';

import i18next from '../services/translate';
import store from '../store';
import MainLayout from '../layouts/MainLayout';
import '../styles/normalize';
import LoginPanel from '../containers/LoginPanel/LoginPanel';
import history from '../utils/history';

const app_container = document.getElementById('app');

render(
  <I18nextProvider i18n={i18next}>
    <Provider store={store}>
      <MainLayout>
        <Router history={history}>
          <Switch>
            <Route component={LoginPanel} />
          </Switch>
        </Router>
      </MainLayout>
    </Provider>
  </I18nextProvider>,
  app_container
);
