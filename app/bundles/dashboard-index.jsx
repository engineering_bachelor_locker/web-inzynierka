import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { I18nextProvider } from 'react-i18next';
import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import MomentUtils from '@date-io/moment';

import i18next from '../services/translate';
import store from '../store';
import history from '../utils/history';
import DashboardLayout from '../layouts/DashboardLayout';
import '../styles/normalize';
import Dashboard from '../containers/Dashboard/Dashboard';
import '../utils/format';

const app_container = document.getElementById('app');

render(
  <I18nextProvider i18n={i18next}>
    <Provider store={store}>
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <Router history={history}>
          <DashboardLayout>
            <Dashboard />
          </DashboardLayout>
        </Router>
      </MuiPickersUtilsProvider>
    </Provider>
  </I18nextProvider>,
  app_container
);
