import React from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';

const PrimaryButton = ({
  className,
  children,
  fullWidth,
  href,
  variant,
  disabled,
  ...params
}) => (
  <div className={className}>
    <Button
      color="primary"
      fullWidth={fullWidth}
      href={href}
      variant={variant || 'contained'}
      disabled={disabled}
      {...params}
    >
      {children}
    </Button>
  </div>
);

export default styled(PrimaryButton)`
  display: inline-block;
  margin: 30px 0;
  width: ${p => (p.fullWidth ? '100%' : 'auto')};
`;

export const SaveButton = styled(PrimaryButton)`
  display: inline-block;
  width: auto;
  button {
    text-transform: none;
    font-size: 0.6rem;
    background-color: #76ff03;
    color: white;
    &:hover {
      background-color: #32cb00;
    }
  }
`;

export const RemoveButton = styled(PrimaryButton)`
  display: inline-block;
  width: auto;
  button {
    text-transform: none;
    font-size: 0.6rem;
    background-color: #e91e63;
    color: white;
    &:hover {
      background-color: #b0003a;
    }
  }
`;

export const EditButton = styled(PrimaryButton)`
  display: inline-block;
  width: auto;
  button {
    text-transform: none;
  }
`;

export const TransparentButton = styled.span`
  display: inline-block;
  color: #05164d;
  font-size: 0.8rem;
  font-weight: 300;
  background-color: none;
  padding: 0 5px;
  height: 48px;
  line-height: 48px;
  width: 100%;
  margin-bottom: 0.6rem;
  box-sizing: border-box;
  text-align: center;
  transition: background-color 0.3s;
  margin-top: 1.5rem;
  border: 1px solid #448aff;
  &:hover {
    cursor: pointer;
    color: white;
    background-color: #448aff;
  }
`;
