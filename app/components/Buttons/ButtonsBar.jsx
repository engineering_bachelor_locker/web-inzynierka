import React from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import StyledIcon from '../MaterialIcon';
import LinearLoader from '../Loaders/LinearProgress';

const ButtonsBar = ({
  className,
  id,
  leftBtnId,
  leftBtnText,
  leftBtnAction,
  removeBtnId,
  removeBtnText,
  removeBtnAction,
  saveBtnId,
  saveBtnText,
  saveBtnAction,
  isLoaderVisible
}) => (
  <div className={className} id={id}>
    <LinearLoader isLoading={isLoaderVisible} />
    <ButtonsInnerWrapper>
      <LeftButton onClick={leftBtnAction} id={leftBtnId}>
        <StyledIcon iconName="keyboard_arrow_left" />
        {leftBtnText}
      </LeftButton>
      <ActionButtonsWrappers>
        {removeBtnText && (
          <RemoveButton
            id={removeBtnId}
            onClick={removeBtnAction}
            style={removeButtonStyle}
            color="primary"
            variant="contained"
            disabled={isLoaderVisible}
          >
            {removeBtnText}
          </RemoveButton>
        )}
        {saveBtnText && (
          <SaveButton
            id={saveBtnId}
            onClick={saveBtnAction}
            style={saveButtonStyle}
            color="primary"
            variant="contained"
            disabled={isLoaderVisible}
          >
            {saveBtnText}
          </SaveButton>
        )}
      </ActionButtonsWrappers>
    </ButtonsInnerWrapper>
  </div>
);

const saveButtonStyle = {
  height: '46px'
};

const removeButtonStyle = {
  height: '46px',
  backgroundColor: 'transparent',
  color: 'rgb(51,51,51)'
};

const ButtonsInnerWrapper = styled.div`
  position: relative;
  display: inline-block;
  width: 100%;
  box-sizing: border-box;
`;

const LeftButton = styled.span`
  line-height: 46px;
  font-size: 0.7rem;
  left: 0;
  color: #376ac7;
  cursor: pointer;
  vertical-align: middle;
  ${StyledIcon} {
    vertical-align: middle;
    font-weight: 300;
    font-size: 1.1rem;
    margin-top: -2px;
  }
`;

const SaveButton = styled(Button)`
  text-transform: none !important;
  font-size: 0.8rem !important;
  display: inline-block;
  width: auto;
`;

const RemoveButton = styled(SaveButton)`
  margin-right: 0.8rem !important;
`;

const ActionButtonsWrappers = styled.div`
  display: inline-block;
  position: absolute;
  right: 0;
`;

const StyledButtonsBar = styled(ButtonsBar)`
  box-sizing: border-box;
  padding: 16px 18px 16px 18px;
  width: 100%;
  position: absolute;
  bottom: 0;
  left: 0;
  background-color: white;
  z-index: 10;
  border-top: #dddddd solid 1px;
  ${LinearLoader} {
    position: absolute;
    top: 0;
    left: 0;
  }
`;

export default StyledButtonsBar;

export const ButtonsBarWrapper = styled.div`
  width: 100%;
  box-sizing: border-box;
  padding: 0 40px;
  position: absolute;
  left: 0;
  bottom: 0;
  ${StyledButtonsBar} {
    position: relative;
  }
`;
