import styled from 'styled-components';

export const PageHeader = styled.h2`
  display: inline-block;
  width: auto;
  line-height: 1.54;
  color: rgba(51, 51, 51, 1);
  font-size: 1.6rem;
  margin-bottom: 0.8rem;
  position: relative;
  &:after {
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 120%;
    background: #448aff;
    height: 2px;
  }
`;

export const PageSubHeader = styled.h5`
  width: 100%;
  font-size: 0.8rem;
  color: rgb(51, 51, 51);
  opacity: 0.9;
  line-height: 1;
  font-weight: 400;
  b {
    position: relative;
    display: inline-block;
    margin: 0 0.2rem;
    background-color: #fafafa;
    border-radius: 3px;
    padding: 0.2rem 0.3rem;
  }
`;
