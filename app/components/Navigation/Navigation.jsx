import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { NavBar, BrandLogo, Email } from './styled-components';

import LogoPG from '../../assets/logo-pg.png';
import LogoChem from '../../assets/logo-chem.png';

import { TransparentButton } from '../Buttons/button';
import { getUserEmail, isUserLoggedIn } from '../../duck/selectors';
import { signOut } from '../../duck/actions';

const mapStateToProps = state => ({
  email: getUserEmail(state),
  isUserLoggedIn: isUserLoggedIn(state)
});

const mapDispatchToProps = dispatch => ({
  signOut: () => dispatch(signOut())
});

class Navigation extends PureComponent {
  render = () => {
    const { t, className, email, isUserLoggedIn } = this.props;
    return (
      <div className={className}>
        <NavBar>
          <BrandLogo logoUrl={LogoPG} link="/" />
          <BrandLogo logoUrl={LogoChem} link="/" />
          <Email>{email}</Email>
          {isUserLoggedIn && (
            <TransparentButton onClick={this.props.signOut}>
              {t('common.logout')}
            </TransparentButton>
          )}
        </NavBar>
      </div>
    );
  };
}

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(Navigation);

const TranslatedComponent = translate()(ConnectedComponent);

export default styled(TranslatedComponent)`
  display: inline-block;
  width: 100%;
  height: 56px;
  position: fixed;
  top: 0;
  z-index: 30;
  ${TransparentButton} {
    width: auto;
    position: absolute;
    right: 0.6rem;
    top: 4px;
    margin: 0;
    padding-left: 2.2rem;
    padding-right: 2.2rem;
  }
`;
