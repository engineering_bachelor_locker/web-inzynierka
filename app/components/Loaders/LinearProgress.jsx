import React from 'react';
import styled from 'styled-components';
import { withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';

const styles = {
  root: {
    display: 'flex',
    minHeight: '5px',
    flexFlow: 'row wrap',
    alignItems: 'stretch'
  },
  child: {
    flex: '1 100%'
  }
};

const LinearProgressComponent = ({ className, classes, color, isLoading }) => (
  <div className={`${classes.root} ${className}`}>
    <div className={classes.child}>
      {isLoading && <LinearProgress color={color || 'primary'} />}
    </div>
  </div>
);

const MaterialStyled = withStyles(styles)(LinearProgressComponent);

export default styled(MaterialStyled)`
  display: inline-block;
  width: 100%;
  box-sizing: border-box;
`;
