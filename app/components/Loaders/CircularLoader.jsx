import React from 'react';
import styled from 'styled-components';
import CircularProgress from '@material-ui/core/CircularProgress';

const CustomizedLoader = ({ className, isVisible, ...rest }) => (
  <LoaderContainer className={className} isVisible={isVisible}>
    <CircularProgress {...rest} />
  </LoaderContainer>
);

const LoaderContainer = styled.span`
  visibility: ${props => (props.isVisible ? 'initial' : 'hidden')};
  opacity: ${props => (props.isVisible ? '1' : '0')};
  transition: opacity 0.5s;
  line-height: 1;
`;

export default styled(CustomizedLoader)``;
