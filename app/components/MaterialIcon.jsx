import React from 'react';
import styled from 'styled-components';

export const Icon = ({ iconName, className, ...props }) => (
  <i className={`material-icons ${className}`} {...props}>
    {iconName}
  </i>
);

export default styled(Icon)``;
