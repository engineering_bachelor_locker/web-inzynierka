import React from 'react';
import styled, { keyframes } from 'styled-components';

const animated_bg = keyframes`
  from {
   background-position: 100% 0;
  }
  to {
   background-position: -100% 0;
  }
`;

const EyeholderBaseComponent = styled.span`
  animation: ${animated_bg} 1s linear;
  animation-fill-mode: forwards;
  animation-iteration-count: infinite;
  background: #f6f7f8;
  background: linear-gradient(to right, #eeeeee 8%, #dddddd 42%, #eeeeee 45%);
  background-size: 200% 100%;
  display: inline-block;
  position: relative;
`;

const FakeLogo = styled(EyeholderBaseComponent)`
  height: 36px;
  width: 200px;
  margin: 10px 25px;
`;

const FakeUser = styled(EyeholderBaseComponent)`
  height: 36px;
  width: 36px;
  border-radius: 50%;
`;

const FakeMenuComponent = ({ className }) => (
  <div className={className}>
    <FakeLogo />
    <FakeUser />
  </div>
);

export const FakeNavigation = styled(FakeMenuComponent)`
  width: 100%;
  display: inline-block;
  z-index: 22;
  background: white;
  box-shadow: 1px 2px 10px 0px rgba(0, 0, 0, 0.32);
  position: fixed;
  top: 0;
  z-index: 30;
  ${FakeUser} {
    position: absolute;
    right: 25px;
    top: 10px;
  }
`;
