import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import MaterialIcon from '../MaterialIcon';

const popupRoot = document.getElementById('popup-root');

class Popup extends PureComponent {
  constructor(props) {
    super(props);
    this.el = document.createElement('div');
    this.popup = React.createRef();
  }

  componentDidMount = () => {
    popupRoot.appendChild(this.el);
    //document.addEventListener('click', this.hideOnClickOutside, false);
  };

  componentWillUnmount = () => {
    //document.addEventListener('click', this.hideOnClickOutside, false);
  };

  render = () => {
    const { className, children } = this.props;
    return ReactDOM.createPortal(
      <div className={className}>
        <PopupContentWrapper innerRef={this.popup}>
          <MaterialIcon
            iconName="clear"
            onClick={() => this.props.handleClose()}
          />
          {children}
        </PopupContentWrapper>
      </div>,
      this.el
    );
  };

  hideOnClickOutside = event => {
    if (!this.popup.current.contains(event.target)) {
      this.props.handleClose();
    }
  };
}

export default styled(Popup)`
  position: absolute;
  z-index: 100;
  background-color: rgba(0, 0, 0, 0.3);
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
`;

const PopupContentWrapper = styled.div`
  display: inline-block;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-80%, -50%);
  background-color: white;
  padding: 0.8rem;
  ${MaterialIcon} {
    position: absolute;
    top: 0.3rem;
    right: 0.3rem;
    cursor: pointer;
  }
`;
