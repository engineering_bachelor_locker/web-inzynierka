import React from 'react';
import styled from 'styled-components';

const Header = styled.h1`
  text-align: center;
  width: 100%;
  display: inline-block;
  font-size: 2.8rem;
  position: absolute;
  color: rgb(85, 85, 85);
  text-transform: uppercase;
  top: 40%;
  left: 0;
  transform: translateY(-50%);
`;

const UnauthorizedPageComponent = ({ t, className }) => (
  <div className={className}>
    <Header>{t('common.unauthorized')}</Header>
  </div>
);

export const UnauthorizedPage = styled(UnauthorizedPageComponent)`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  height: calc(100vh - 56px);
  padding-top: 56px;
  position: relative;
`;

const NotFoundPageComponent = ({ t, className }) => (
  <div className={className}>
    <Header>{t('common.page_not_found')}</Header>
  </div>
);

export const NotFoundPage = styled(NotFoundPageComponent)`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  height: calc(100vh - 56px);
  padding-top: 56px;
  background: rgb(238, 238, 238);
  position: relative;
`;
