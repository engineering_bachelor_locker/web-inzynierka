import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { translate } from 'react-i18next';

const VerticalList = ({ t, className, children, itemList, location }) => (
  <div className={className}>
    <VerticalListItemsContainer>
      {itemList.map((item, index) => (
        <VerticalListItem
          key={index}
          selected={location.pathname.indexOf(item.url) > -1}
          isDisabled={item.disabled}
          id={item.id}
        >
          <Link to={item.url} alt={item.name}>
            {t(item.name)}
          </Link>
        </VerticalListItem>
      ))}
      {children}
    </VerticalListItemsContainer>
  </div>
);

const VerticalListItemsContainer = styled.div`
  border-right: #dddddd solid 1px;
  height: calc(100% - 100px);
  margin: 25px 0;
  padding: 0 calc(15% - 20px);
`;

const VerticalListItem = styled.div`
  line-height: 1.4;
  min-height: 1.6rem;
  font-weight: ${props => (props.selected ? `600` : `300`)};
  font-size: 0.8rem;
  pointer-events: ${props => (props.isDisabled ? 'none' : 'initial')};
  position: relative;
  padding-left: 20px;
  > span,
  a {
    padding: 0.4rem 0;
    display: inline-block;
    color: ${p => (p.isDisabled ? 'rgb(103, 103, 103)' : 'rgb(51, 51, 51)')};
  }
  &:before {
    content: '';
    position: absolute;
    width: 5px;
    height: 1.6rem;
    left: 0px;
    top: 50%;
    transform: translateY(-50%);
    background: ${p => (p.selected ? '#448aff' : '')};
  }
  &:hover {
    cursor: pointer;
  }
`;

const Translated = translate()(VerticalList);

export default styled(Translated)`
  height: 100%;
  width: 100%;
  background-color: white;
  position: relative;
`;
