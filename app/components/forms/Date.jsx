import React, { PureComponent } from 'react';
import { DateTimePicker } from 'material-ui-pickers';
import styled from 'styled-components';
import moment from 'moment';
import { InputAdornment } from '@material-ui/core';
import MaterialIcon from '../MaterialIcon';

moment.locale('pl');

class Date extends PureComponent {
  render() {
    const {
      className,
      id,
      name,
      value,
      label,
      helperText,
      initialFocusedDate
    } = this.props;

    return (
      <div className={className}>
        <DateTimePicker
          id={id}
          ampm={false}
          showTabs={false}
          autoSubmit={false}
          allowKeyboardControl={true}
          disablePast
          value={value}
          label={label}
          initialFocusedDate={initialFocusedDate}
          format="DD.MM.YYYY HH:mm"
          onChange={this.handleDateChange.bind(this, name)}
          helperText={helperText}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <MaterialIcon iconName="date_range" />
              </InputAdornment>
            )
          }}
        />
      </div>
    );
  }

  handleDateChange = (name, value) => {
    this.props.onChange(name, value.format());
  };
}

export default styled(Date)`
  width: 100%;
  display: inline-block;
`;
