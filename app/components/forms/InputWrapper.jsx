import styled from 'styled-components';
import { grid } from '../../styles/grid';

export const InputWrapper = styled.div`
  display: inline-block;
  min-height: 2px;
  ${p => grid.breakpoints({ df: 12, m: p.width }, 12, '15px')};
`;

export const InputsRowWrapper = styled.div`
  display: inline-block;
  padding: 0;
  margin-left: -15px;
  margin-right: -15px;
  width: calc(100% + 30px);
`;
