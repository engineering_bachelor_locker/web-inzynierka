import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import styled from 'styled-components';

class TextInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pristine: true,
      valid: true,
      visible: false
    };
  }

  componentDidMount = () => {
    if (!this.props.pristine || !this.state.pristine) {
      const valid = this.props.validate(this.props.value);
      this.setState({ valid });
    }
  };

  componentWillReceiveProps = nextProps => {
    if (!nextProps.pristine || !this.state.pristine) {
      const valid = this.props.validate(nextProps.value);
      this.setState({ valid });
    }
  };

  render() {
    const {
      className,
      name,
      type,
      label,
      id,
      value,
      multiline,
      rows,
      rowsMax,
      onChange,
      helperText,
      errorText,
      ...props
    } = this.props;
    const { valid } = this.state;
    const hintText = valid ? helperText : errorText;
    return (
      <div className={className}>
        <TextInputWrapper>
          <TextField
            id={id}
            name={name}
            type={type || 'text'}
            error={!valid}
            value={value}
            label={label}
            multiline={multiline}
            rows={rows}
            rowsMax={rowsMax}
            onChange={event => onChange(name, event.target.value)}
            onBlur={this.handleBlur}
            helperText={hintText}
            {...props}
          />
          {this.props.children}
        </TextInputWrapper>
      </div>
    );
  }

  handleBlur = () => {
    const valid = this.props.validate(this.props.value);
    this.setState({ pristine: false, valid });
  };
}

export default styled(TextInput)``;

const TextInputWrapper = styled.div`
  position: relative;
  margin: 8px 0;
  input::-webkit-credentials-auto-fill-button {
    visibility: hidden;
    pointer-events: none;
  }
`;
