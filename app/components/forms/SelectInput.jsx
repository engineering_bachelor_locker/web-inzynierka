import React, { Component } from 'react';
import styled from 'styled-components';
import { translate } from 'react-i18next';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  }
});

@translate()
class SelectComponent extends Component {
  render = () => {
    const {
      t,
      id,
      className,
      classes,
      label,
      name,
      selectValues,
      multiple,
      value,
      disabled,
      helperText
    } = this.props;

    return (
      <div className={className}>
        <FormControl className={classes.formControl} disabled={disabled}>
          <InputLabel htmlFor={id} shrink={value}>
            {label}
          </InputLabel>
          <Select
            id={id}
            multiple={multiple}
            value={value}
            onChange={this.handleChange.bind(this, name)}
            inputProps={{
              name: name,
              id: id,
              value
            }}
          >
            {selectValues.map((item, i) => (
              <MenuItem value={item.value} key={i}>
                {t(item.name)}
              </MenuItem>
            ))}
          </Select>
          <FormHelperText>{t(helperText)}</FormHelperText>
        </FormControl>
      </div>
    );
  };

  handleChange = (name, event) => this.props.onChange(name, event.target.value);
}

const MaterialStyled = withStyles(styles)(SelectComponent);

export default styled(MaterialStyled)`
  display: inline-block;
  position: relative;
  & > div {
    margin: 0;
  }
`;
