import React, { Component } from 'react';
import styled from 'styled-components';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

class SwitchComponent extends Component {
  render = () => {
    const {
      classNames,
      activeLabel,
      inactiveLabel,
      name,
      value,
      switchStates
    } = this.props;

    const states = switchStates || {};
    const checked = states[value];
    const switch_value = checked
      ? states.switch_inactive
      : states.switch_active;

    return (
      <div className={classNames}>
        <FormGroup row>
          <FormControlLabel
            control={
              <Switch
                color="primary"
                checked={checked}
                onChange={() => this.props.onChange(name, switch_value)}
              />
            }
            label={checked ? activeLabel : inactiveLabel}
          />
        </FormGroup>
      </div>
    );
  };
}

export default styled(SwitchComponent)`
  display: inline-block;
  width: 100%;
`;
