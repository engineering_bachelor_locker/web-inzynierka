import styled from 'styled-components';

export const Banner = styled.div`
  width: 100%;
  height: 100%;
  background-image: url(${props => props.backgroundImage});
  background-repeat: no-repeat;
  background-position: center center;
  background-size: cover;
  transition: all 0.4s;
`;
