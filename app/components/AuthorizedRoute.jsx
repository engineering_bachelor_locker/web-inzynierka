import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import { getUserRole, isUserProfileDataFetched } from '../duck/selectors';
import { hasAccessToPath } from '../utils/userAccess';
import { appUrls } from '../services/appUrls';

const mapStateToProps = state => ({
  userRole: getUserRole(state),
  isUserProfileFetched: isUserProfileDataFetched(state)
});

const mapDispatchToProps = () => ({});

class AuthorizedRoute extends Component {
  render() {
    const {
      component: Component,
      userRole,
      isUserProfileFetched,
      path
    } = this.props;

    const accessible = hasAccessToPath(userRole, path);

    return !isUserProfileFetched ? (
      ''
    ) : accessible ? (
      <Route component={Component} path={path} {...this.props} />
    ) : (
      <Redirect to={{ pathname: appUrls.DASHBOARD }} />
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthorizedRoute);
