import React, { Component } from 'react';
import styled from 'styled-components';
import { translate } from 'react-i18next';

import {
  CloseButton,
  MessageContainer,
  MessageContent,
  MessageTypeIcon
} from './styled-components';
import StyledIcon from '../MaterialIcon';

@translate()
class Notification extends Component {
  render = () => {
    const { t, className, type, content, uuid } = this.props;
    const text = content && content.text ? content.text : content;
    const options = content && content.options ? content.options : {};

    return (
      <div className={className}>
        <CloseButton>
          <StyledIcon
            iconName="close"
            onClick={() => this.props.onClose(uuid)}
          />
        </CloseButton>
        <MessageContainer>
          <MessageTypeIcon type={type}>
            <StyledIcon iconName={type.icon} />
          </MessageTypeIcon>
          <MessageContent
            dangerouslySetInnerHTML={{ __html: t(text, options) }}
          />
        </MessageContainer>
      </div>
    );
  };
}

export default styled(Notification)`
  box-shadow: 8px -1px 48px rgba(0, 0, 0, 0.15),
    2px 6px 13px rgba(0, 0, 0, 0.24);
  position: relative;
  background-color: white;
  min-width: 100px;
  margin-bottom: 0.6rem;
  padding: 28px 18px;
  line-height: 1.1;
  border-radius: 4px;
  right: 0;
  a {
    color: rgb(43, 103, 213);
  }
`;
