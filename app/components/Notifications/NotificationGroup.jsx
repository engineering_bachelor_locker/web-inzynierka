import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

import Notification from './Notification';

class NotificationGroup extends PureComponent {
  render = () => {
    const { className, items } = this.props;
    return (
      <div className={className}>
        <TransitionGroup className="notification-group">
          {items.map(item => (
            <CSSTransition
              key={item.uuid}
              timeout={500}
              classNames="notification"
            >
              <Notification
                key={item.uuid}
                uuid={item.uuid}
                type={item.notification.type}
                content={item.notification.content}
                onClose={uuid => this.props.removeNotification(uuid)}
              />
            </CSSTransition>
          ))}
        </TransitionGroup>
      </div>
    );
  };
}

export default styled(NotificationGroup)`
  display: ${props => (props.isEmpty ? 'none' : 'block')};
  z-index: 9999;
  max-width: 660px;
  width: 40%;
  height: auto;
  position: fixed;
  top: 90px;
  right: 20px;
  & > ol {
    list-style-type: none;
    padding: 0;
    margin: 0;
  }
`;
