import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import NotificationGroup from './NotificationGroup';

import { getNotifications } from '../../duck/selectors';
import { removeNotification } from '../../duck/actions';

const notificationsRoot = document.getElementById('notifications-root');

const mapStateToProps = state => ({
  items: getNotifications(state)
});

const mapDispatchToProps = dispatch => ({
  removeNotification: index => dispatch(removeNotification(index))
});

@connect(
  mapStateToProps,
  mapDispatchToProps
)
export default class NotificationsContainer extends PureComponent {
  constructor(props) {
    super(props);

    this.el = document.createElement('div');
  }

  componentDidMount() {
    notificationsRoot.appendChild(this.el);
  }

  render = () =>
    ReactDOM.createPortal(
      <NotificationGroup
        items={this.props.items}
        removeNotification={this.props.removeNotification}
      />,
      this.el
    );
}
