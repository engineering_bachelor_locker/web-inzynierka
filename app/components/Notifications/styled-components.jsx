import styled from 'styled-components';

export const MessageContainer = styled.div`
  position: relative;
`;

export const CloseButton = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  margin: 6px;
  font-size: 1rem;
  cursor: pointer;
  & > * {
    font-size: 1rem;
    color: #8a8a8a;
  }
`;

export const MessageContent = styled.span`
  color: rgb(85, 85, 85);
  display: inline-block;
  font-size: 0.75rem;
  margin-left: 10%;
  line-height: 120%;
  width: 86%;
`;

export const MessageTypeIcon = styled.div`
  position: absolute;
  display: inline-block;
  transform: translate(0%, -30%);
  top: 50%;
  left: 6px;
  width: 10%;
  color: ${props => props.type.color};
  & > * {
    font-size: 0.925rem;
    :focus {
      outline: none;
    }
  }
`;
