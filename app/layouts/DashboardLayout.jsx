import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';

import mainMuiTheme from '../styles/muiTheme';
import Navigation from '../components/Navigation/Navigation';
import AbstractBackground from '../assets/abstract-banner.jpg';
import { fetchProfileInformation } from '../duck/actions';
import { isUserProfileDataFetched } from '../duck/selectors';
import { FakeNavigation } from '../components/Eyeholders';
import NotificationsContainer from '../components/Notifications/NotificationContainer';

const mapStateToProps = state => ({
  isUserProfileFetched: isUserProfileDataFetched(state)
});

const mapDispatchToProps = dispatch => ({
  fetchProfileInformation: () => dispatch(fetchProfileInformation())
});

class DashboardLayout extends Component {
  componentDidMount = () => {
    this.props.fetchProfileInformation();
  };

  render = () => {
    const { className, children, isUserProfileFetched } = this.props;
    return (
      <MuiThemeProvider theme={mainMuiTheme}>
        <div className={className}>
          <NotificationsContainer />
          {isUserProfileFetched ? <Navigation /> : <FakeNavigation />}
          {isUserProfileFetched ? children : ''}
        </div>
      </MuiThemeProvider>
    );
  };
}

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardLayout);

export default styled(withRouter(ConnectedComponent))`
  width: 100%;
  height: 100vh;
  box-sizing: border-box;
  background-image: url(${AbstractBackground});
  background-repeat: no-repeat;
  background-position: center center;
  background-size: cover;
`;
