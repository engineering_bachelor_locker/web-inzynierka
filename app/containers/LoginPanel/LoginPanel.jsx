import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';

import { Panel, PanelTitle, PanelText } from './styled-components';
import TextInput from '../../components/forms/TextInput';
import Button from '../../components/Buttons/button';
import Loader from '../../components/Loaders/CircularLoader';

import { signIn } from '../../duck/actions';
import { isUserLogging } from '../../duck/selectors';

const mapStateToProps = state => ({
  isLoading: isUserLogging(state)
});
const mapDispatchToProps = dispatch => ({
  signIn: (login, password) => dispatch(signIn(login, password))
});

class LoginPanel extends PureComponent {
  state = {
    login: '',
    password: ''
  };

  render = () => {
    const { t, className, isLoading } = this.props;
    const { login, password } = this.state;

    return (
      <div className={className}>
        <Panel>
          <PanelTitle>{t('login_panel.title')}</PanelTitle>
          <PanelText>{t('login_panel.subtitle')}</PanelText>
          <form onSubmit={this.handleSignIn}>
            <TextInput
              label={t('login_panel.login')}
              fullWidth
              type="text"
              name="login"
              id="login_panel_login"
              errorText={t('forms.invalid_email')}
              pristine={true}
              value={login}
              validate={value => value}
              onChange={(name, value) => this.setState({ [name]: value })}
            />
            <TextInput
              label={t('login_panel.password')}
              fullWidth
              type="password"
              name="password"
              id="login_panel_password"
              validate={value => value}
              errorText={t('forms.invalid_password')}
              pristine={true}
              value={password}
              onChange={(name, value) => this.setState({ [name]: value })}
            />
            <Button onClick={this.handleSignIn} type="submit" fullWidth>
              {t('login_panel.button_text')}
              <Loader isVisible={isLoading} color="secondary" size={20} />
            </Button>
          </form>
        </Panel>
      </div>
    );
  };

  handleSignIn = e => {
    e.preventDefault();
    const { login, password } = this.state;
    if (login && password) {
      this.props.signIn(login, password);
    }
  };
}

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPanel);

const TranslatedComponent = translate()(ConnectedComponent);

export default styled(TranslatedComponent)`
  height: 100%;
  display: inline-block;
  box-sizing: border-box;
  padding-top: 56px;
  ${Button} {
    position: relative;
    ${Loader} {
      position: absolute;
      right: 1.2rem;
    }
  }
`;
