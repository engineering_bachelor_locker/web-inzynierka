import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import styled from 'styled-components';
import { PageHeader } from '../../components/Typography/Headers';

const mapStateToProps = () => ({});
const mapDispatchToProps = () => ({});

class Users extends PureComponent {
  render = () => {
    const { t, className } = this.props;
    return (
      <div className={className}>
        <PageHeader>{t('users.title')}</PageHeader>
      </div>
    );
  };
}

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(Users);
const TranslatedComponent = translate()(ConnectedComponent);

export default styled(TranslatedComponent)`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  background-color: white;
  padding: 1.2rem 1.6rem;
`;
