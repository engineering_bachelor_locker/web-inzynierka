const EQUIPMENTS_FETCHED = 'Equipment/EQUIPMENTS_FETCHED';
const EQUIPMENTS_STATE_CHANGED = 'Equipment/EQUIPMENTS_STATE_CHANGED';
const EQUIPMENT_DATA_FETCHED = 'Equipment/EQUIPMENT_DATA_FETCHED';
const EQUIPMENT_FORM_STATE_CHANGED = 'Equipment/EQUIPMENT_FORM_STATE_CHANGED';
const FORM_FIELD_SET = 'Equipment/FORM_FIELD_SET';
const FORM_CLEARED = 'Equipment/FORM_CLEARED';

export default {
  EQUIPMENTS_FETCHED,
  EQUIPMENTS_STATE_CHANGED,
  EQUIPMENT_DATA_FETCHED,
  EQUIPMENT_FORM_STATE_CHANGED,
  FORM_FIELD_SET,
  FORM_CLEARED
};
