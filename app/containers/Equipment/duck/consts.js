import { equipment_states } from '../../../duck/consts';

export const equipment_switch_states = {
  [equipment_states.ACTIVE]: true,
  [equipment_states.INACTIVE]: false,
  switch_active: equipment_states.ACTIVE,
  switch_inactive: equipment_states.INACTIVE
};
