import types from './types';
import { EquipmentList, Equipment } from './records';

export const equipments = (state = new EquipmentList(), action) => {
  switch (action.type) {
    case types.EQUIPMENTS_FETCHED:
      return state.set('list', action.list);
    case types.EQUIPMENTS_STATE_CHANGED:
      return state.set('list_state', action.state);
    case types.EQUIPMENT_FORM_STATE_CHANGED:
      return state.set('form_state', action.state);
    case types.EQUIPMENT_DATA_FETCHED:
      return state.setIn(['equipment_form'], action.equipment_form);
    case types.FORM_FIELD_SET:
      return state.setIn(['equipment_form', action.name], action.value);
    case types.FORM_CLEARED:
      return state.set('equipment_form', new Equipment());
  }

  return state;
};
