import { message_types, api_states } from '../../../duck/consts';
import RestClient from '../../../services/RestClient';
import { addNotification, redirect } from '../../../duck/actions';
import { appUrls } from '../../../services/appUrls';
import types from './types';
import { normalizeEquipmentList, normalizeEquipmentData } from './normalizers';
import { getEquipmentFormData } from './selectors';
import history from '../../../utils/history';

export const saveEquipment = room_id => (dispatch, getState) => {
  const data = getEquipmentFormData(getState());
  dispatch(equipmentFormStateChanged(api_states.IN_PROGRESS));
  return RestClient.saveEquipment(data, room_id).then(
    () => {
      dispatch(equipmentFormStateChanged(api_states.DONE));
      history.goBack();
    },
    () => {
      dispatch(equipmentFormStateChanged(api_states.DONE));
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};

export const removeEquipment = () => (dispatch, getState) => {
  const data = getEquipmentFormData(getState());
  dispatch(equipmentFormStateChanged(api_states.IN_PROGRESS));
  return RestClient.removeEquipment(data.room_id, data.id).then(
    () => {
      dispatch(equipmentFormStateChanged(api_states.DONE));
      dispatch(redirect(appUrls.EQUIPMENTS));
    },
    () => {
      dispatch(equipmentFormStateChanged(api_states.DONE));
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};

export const fetchEquipmentList = () => dispatch => {
  dispatch(equipmentsStateChanged(api_states.IN_PROGRESS));
  return RestClient.getEquipments().then(
    response => {
      dispatch(equipmentsStateChanged(api_states.DONE));
      const normalized = normalizeEquipmentList(response);
      dispatch({
        type: types.EQUIPMENTS_FETCHED,
        list: normalized
      });
    },
    () => {
      dispatch(equipmentsStateChanged(api_states.DONE));
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};

export const equipmentsStateChanged = state => ({
  type: types.EQUIPMENTS_STATE_CHANGED,
  state
});

export const fetchEquipment = (room_id, id) => dispatch => {
  dispatch(equipmentFormStateChanged(api_states.IN_PROGRESS));
  return RestClient.getEquipment(room_id, id).then(
    response => {
      dispatch(equipmentFormStateChanged(api_states.DONE));
      const normalized = normalizeEquipmentData(response);
      dispatch({
        type: types.EQUIPMENT_DATA_FETCHED,
        equipment_form: normalized
      });
    },
    () => {
      dispatch(equipmentFormStateChanged(api_states.DONE));
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};

export const equipmentFormStateChanged = state => ({
  type: types.EQUIPMENT_FORM_STATE_CHANGED,
  state
});

export const setFormField = (name, value) => ({
  type: types.FORM_FIELD_SET,
  name,
  value
});

export const clearFormData = () => ({
  type: types.FORM_CLEARED
});
