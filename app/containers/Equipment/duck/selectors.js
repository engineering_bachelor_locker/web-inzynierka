import { api_states } from '../../../duck/consts';

export const getEquipmentList = state => state.equipments.list;

export const isEquipmentListLoading = state =>
  state.equipments.list_state === api_states.IN_PROGRESS;

export const isEquipmentFormLoading = state =>
  state.equipments.form_state === api_states.IN_PROGRESS;

export const getEquipmentFormData = state =>
  state.equipments.equipment_form.toJS();
