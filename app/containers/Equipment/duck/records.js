import { Record } from 'immutable';
import { api_states, room_states } from '../../../duck/consts';

export const Equipment = Record({
  id: null,
  room_id: null,
  name: '',
  number: '',
  state: room_states.INACTIVE,
  description: ''
});

export const EquipmentList = Record({
  list: [],
  equipment_form: new Equipment(),
  list_state: api_states.DONE,
  form_state: api_states.DONE
});
