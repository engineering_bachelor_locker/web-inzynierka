import { Equipment } from './records';

export const normalizeEquipmentList = response => {
  const equipments = response.data.map(
    equipment => new Equipment({ ...equipment })
  );
  return equipments;
};

export const normalizeEquipmentData = response => {
  const { id } = response.data.room || {};
  return new Equipment({ ...response.data, room_id: id });
};
