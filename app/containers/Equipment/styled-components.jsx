import styled from 'styled-components';
import React from 'react';
import { Link } from 'react-router-dom';
import EquipmentIcon from '../../assets/equipment.png';
import { appUrls } from '../../services/appUrls';

const Icon = styled.div`
  display: inline-block;
  width: 35px;
  height: 35px;
  background-image: url(${EquipmentIcon});
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  vertical-align: middle;
`;

const EquipmentName = styled.span`
  display: inline-block;
  box-sizing: border-box;
  font-size: 0.8rem;
  font-weight: 600;
  margin-left: 0.6rem;
  color: rgb(51, 51, 51);
  vertical-align: middle;
`;

const EquipmentComponent = ({ className, id, room_id, name, canEdit }) => {
  const Wrapper = canEdit ? Link : 'div';
  return (
    <Wrapper to={appUrls.ROOM_EQUIPMENT.format({ id, room_id })}>
      <div className={className}>
        <Icon />
        <EquipmentName>{name}</EquipmentName>
      </div>
    </Wrapper>
  );
};

export const Equipment = styled(EquipmentComponent)`
  display: inline-block;
  width: 100%;
  box-sizing: border-box;
  border: 1px dashed rgb(51, 51, 51);
  border-radius: 5px;
  margin: 0.6rem 0;
  padding: 0.4rem 0.8rem;
  transition: all 0.3s;
  position: relative;
  ${({ canEdit }) =>
    canEdit &&
    `
    cursor: pointer;
    &:hover {
      transform: translate(-2px, -2px);
      border: 1px dashed #3d5afe;
    }
  `}
`;

export const ScrollbarsWrapper = styled.div`
  width: 100%;
  height: 100%;
  padding: 1.2rem 1.6rem;
  box-sizing: border-box;
  display: inline-block;
  position: relative;
`;

export const FormWrapper = styled(ScrollbarsWrapper)`
  position: relative;
  padding: 0;
`;

export const EquipmentsWrapper = styled.div`
  display: inline-block;
  width: 100%;
`;
