import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import styled from 'styled-components';
import { Scrollbars } from 'react-custom-scrollbars';

import TextInput from '../../components/forms/TextInput';
import Switch from '../../components/forms/Switch';
import history from '../../utils/history';

import {
  InputWrapper,
  InputsRowWrapper
} from '../../components/forms/InputWrapper';
import { PageHeader } from '../../components/Typography/Headers';
import ButtonsBar, {
  ButtonsBarWrapper
} from '../../components/Buttons/ButtonsBar';
import { FormWrapper, ScrollbarsWrapper } from './styled-components';
import { NEW_FORM_ID } from '../../duck/consts';
import { equipment_switch_states } from './duck/consts';
import {
  saveEquipment,
  removeEquipment,
  fetchEquipment,
  setFormField,
  clearFormData
} from './duck/actions';

import { isEquipmentFormLoading, getEquipmentFormData } from './duck/selectors';

const mapStateToProps = state => ({
  isLoading: isEquipmentFormLoading(state),
  formData: getEquipmentFormData(state)
});

const mapDispatchToProps = dispatch => ({
  saveEquipment: room_id => dispatch(saveEquipment(room_id)),
  removeEquipment: () => dispatch(removeEquipment()),
  fetchEquipment: (room_id, id) => dispatch(fetchEquipment(room_id, id)),
  setFormField: (name, value) => dispatch(setFormField(name, value)),
  clearFormData: () => dispatch(clearFormData()),
  redirect: () => history.goBack()
});

class EquipmentForm extends PureComponent {
  componentDidMount = () => {
    const { room_id, id } = this.props.match.params;
    if (id && id !== NEW_FORM_ID) {
      this.props.fetchEquipment(room_id, id);
    }
  };

  componentWillUnmount = () => this.props.clearFormData();

  render = () => {
    const { t, className, isLoading, formData, setFormField } = this.props;
    const { name, number, state, description } = formData;
    const { room_id } = this.props.match.params;
    return (
      <div className={className}>
        <FormWrapper>
          <Scrollbars>
            <ScrollbarsWrapper>
              <PageHeader>{t('equipment.form_header')}</PageHeader>
              <InputsRowWrapper>
                <InputWrapper width={8}>
                  <TextInput
                    label={t('equipment.equipment_name')}
                    fullWidth
                    type="text"
                    name="name"
                    id="equipment_name"
                    helperText={t('equipment.equipment_name_hint')}
                    validate={() => true}
                    pristine={true}
                    value={name}
                    onChange={(name, value) => setFormField(name, value)}
                  />
                </InputWrapper>
                <InputWrapper width={4}>
                  <TextInput
                    label={t('equipment.equipment_number')}
                    fullWidth
                    type="text"
                    name="number"
                    id="equipment_name"
                    helperText={t('equipment.equipment_number_hint')}
                    validate={() => true}
                    pristine={true}
                    value={number}
                    onChange={(name, value) => setFormField(name, value)}
                  />
                </InputWrapper>
              </InputsRowWrapper>
              <InputsRowWrapper>
                <InputWrapper width={10}>
                  <Switch
                    activeLabel={t('equipment.equipment_active_state')}
                    inactiveLabel={t('equipment.equipment_inactive_state')}
                    switchStates={equipment_switch_states}
                    value={state}
                    name="state"
                    onChange={(name, value) => setFormField(name, value)}
                  />
                </InputWrapper>
              </InputsRowWrapper>
              <InputsRowWrapper>
                <InputWrapper width={10}>
                  <TextInput
                    label={t('equipment.equipment_description')}
                    fullWidth
                    multiline
                    rows={5}
                    type="text"
                    name="description"
                    id="equipment_description"
                    helperText={t('equipment.equipment_description_hint')}
                    validate={() => true}
                    pristine={true}
                    value={description}
                    onChange={(name, value) => setFormField(name, value)}
                  />
                </InputWrapper>
              </InputsRowWrapper>
            </ScrollbarsWrapper>
          </Scrollbars>
        </FormWrapper>
        <ButtonsBarWrapper>
          <ButtonsBar
            id="payment_settings_btn_bar"
            leftBtnId="go_to_dashboard_btn"
            leftBtnText={t('common.go_back')}
            leftBtnAction={this.props.redirect}
            removeBtnId="equipment_remove_button"
            removeBtnText={t('common.remove')}
            removeBtnAction={this.props.removeEquipment}
            saveBtnId="equipment_save_button"
            saveBtnText={t('common.save')}
            saveBtnAction={() => this.props.saveEquipment(room_id)}
            isLoaderVisible={isLoading}
          />
        </ButtonsBarWrapper>
      </div>
    );
  };
}

const TranslatedComponent = translate()(EquipmentForm);

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(TranslatedComponent);

export default styled(ConnectedComponent)`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  background-color: white;
  overflow: hidden;
  position: relative;
  ${ScrollbarsWrapper} {
    height: calc(100% - 100px);
  }
`;
