import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import styled from 'styled-components';
import { Scrollbars } from 'react-custom-scrollbars';
import { grid } from '../../styles/grid';
import LinearLoader from '../../components/Loaders/LinearProgress';
import { PageHeader } from '../../components/Typography/Headers';
import {
  ScrollbarsWrapper,
  Equipment,
  EquipmentsWrapper
} from './styled-components';

import { getEquipmentList, isEquipmentListLoading } from './duck/selectors';
import { isAdmin } from '../../duck/selectors';
import { fetchEquipmentList } from './duck/actions';

const mapStateToProps = state => ({
  equipments: getEquipmentList(state),
  isLoading: isEquipmentListLoading(state),
  isAdmin: isAdmin(state)
});

const mapDispatchToProps = dispatch => ({
  fetchEquipmentList: () => dispatch(fetchEquipmentList())
});

class EquipmentList extends PureComponent {
  componentDidMount = () => {
    this.props.fetchEquipmentList();
  };

  render = () => {
    const { t, className, equipments, isLoading, isAdmin } = this.props;

    return (
      <div className={className}>
        <LinearLoader isLoading={isLoading} />
        <Scrollbars>
          <ScrollbarsWrapper>
            <PageHeader>{t('equipment.title')}</PageHeader>
            <EquipmentsWrapper>
              {equipments.map(equipment => (
                <Equipment
                  key={equipment.id}
                  {...equipment.toJS()}
                  canEdit={isAdmin}
                />
              ))}
            </EquipmentsWrapper>
          </ScrollbarsWrapper>
        </Scrollbars>
      </div>
    );
  };
}

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(EquipmentList);

const TranslatedComponent = translate()(ConnectedComponent);

export default styled(TranslatedComponent)`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  background-color: white;
  position: relative;
  ${EquipmentsWrapper} {
    ${grid.collapse('20px')}
  }
  ${Equipment} {
    ${grid.breakpoints({ df: 12 }, 12, '20px')}
  }
  ${LinearLoader} {
    position: absolute;
    top: 0;
  }
`;
