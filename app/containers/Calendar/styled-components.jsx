import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import MaterialIcon from '../../components/MaterialIcon';
import EditButton from '../../components/Buttons/button';

export const CalendarWrapper = styled.div`
  display: inline-block;
  width: 100%;
  height: calc(100% - 4.2rem);
  box-sizing: border-box;
`;

const EquipmentName = styled.span`
  display: inline-block;
  width: 100%;
  font-size: 1.2rem;
  font-weight: 600;
  position: relative;
  margin-bottom: 1.6rem;
  line-height: 1.2;
  &:after {
    content: '';
    position: absolute;
    bottom: -3px;
    left: 0;
    width: 100%;
    background: #448aff;
    height: 2px;
  }
`;

const UserName = styled.span`
  display: inline-block;
  width: 100%;
  color: rgb(51, 51, 51);
  font-size: 0.85rem;
  margin-right: 0.6rem;
  font-weight: 600;
  margin-bottom: 0.8rem;
`;

const TimeEntry = styled(UserName)`
  font-style: italic;
  font-weight: 400;
  width: auto;
  vertical-align: middle;
  margin-bottom: 0;
`;

const RowWrapper = styled.div`
  display: inline-block;
  width: 100%;
  margin-bottom: 0.3rem;
`;

const Note = styled(TimeEntry)`
  margin-top: 0.8rem;
`;

const ReservationSummaryComponent = ({
  className,
  equipmentName,
  userName,
  readableStart,
  readableEnd,
  description
}) => (
  <div className={className}>
    {equipmentName && <EquipmentName>{equipmentName}</EquipmentName>}
    {userName && <UserName>{userName}</UserName>}
    <RowWrapper>
      <MaterialIcon iconName="access_time" />
      <TimeEntry>{readableStart}</TimeEntry>
    </RowWrapper>
    <RowWrapper>
      <MaterialIcon iconName="access_time" />
      <TimeEntry>{readableEnd}</TimeEntry>
    </RowWrapper>
    {description && <Note>{description}</Note>}
  </div>
);

export const ReservationSummary = styled(ReservationSummaryComponent)`
  display: inline-block;
  width: 100%;
  box-sizing: border-box;
  padding: 1.6rem 0.8rem;
  min-width: 250px;
  max-width: 350px;
  ${MaterialIcon} {
    vertical-align: middle;
    margin-right: 0.3rem;
    position: relative;
    font-size: 0.9rem;
    color: #448aff;
    top: -2px;
  }
`;

const EditReservationComponent = ({ className, url, children }) => (
  <div className={className}>
    <Link to={url}>
      <EditButton>
        {children}
        <MaterialIcon iconName="create" />
      </EditButton>
    </Link>
  </div>
);

export const EditReservation = styled(EditReservationComponent)`
  ${MaterialIcon} {
    margin-left: 0.3rem;
  }
  position: absolute;
  left: 0.6rem;
  bottom: 1.2rem;
  width: 100%;
  ${EditButton} {
    margin-bottom: 0;
  }
`;

export const NewReservation = styled(EditReservation)`
  position: relative;
`;
