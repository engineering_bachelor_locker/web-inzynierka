import moment from 'moment';
import sample from 'lodash.sample';
import { Reservation } from './records';
import { EQUIPMENT_EVENTS_COLORS } from './consts';

export const normalizeReservationsList = response => {
  const reservations = response.data || [];
  return reservations.map(reservation => {
    const user = reservation.user;
    const userName = `${user.first_name} ${user.last_name}`;

    return new Reservation({
      ...reservation,
      title: `${userName}`,
      equipmentName: reservation.equipment.name,
      userName,
      start: moment(reservation.starts_at).toDate(),
      end: moment(reservation.ends_at).toDate(),
      readableStart: moment(reservation.starts_at).format('DD.MM.YYYY HH:mm'),
      readableEnd: moment(reservation.ends_at).format('DD.MM.YYYY HH:mm'),
      eventColor: assignColorToEvent(reservation.equipment.id)
      //allDay: false
    });
  });
};

const assignedEvent = {};

const assignColorToEvent = id => {
  assignedEvent[id] = assignedEvent[id]
    ? assignedEvent[id]
    : sample(EQUIPMENT_EVENTS_COLORS);
  return assignedEvent[id];
};
