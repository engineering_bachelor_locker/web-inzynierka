import { api_states } from '../../../duck/consts';

export const isCalendarLoading = state =>
  state.calendar.state === api_states.IN_PROGRESS;

export const getReservationList = state => state.calendar.reservations;
