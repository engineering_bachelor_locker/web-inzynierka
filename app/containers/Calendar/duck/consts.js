import moment from 'moment';

export const VIEWS = ['month', 'week', 'day'];

export const MIN_DAY_TIME = moment('6:00am', 'h:mma').toDate();

export const MAX_DAY_TIME = moment('9:00pm', 'h:mma').toDate();

export const EQUIPMENT_EVENTS_COLORS = [
  '#3F51B5',
  '#2196F3',
  '#03A9F4',
  '#00BCD4',
  '#009688',
  '#4CAF50',
  '#8BC34A',
  '#CDDC39',
  '#FFEB3B',
  '#FFC107',
  '#FF9800',
  '#FF5722',
  '#9E9E9E',
  '#607D8B',
  '#673AB7'
];
