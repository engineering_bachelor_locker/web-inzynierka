import { message_types, api_states } from '../../../duck/consts';
import { addNotification } from '../../../duck/actions';
import RestClient from '../../../services/RestClient';
import types from './types';
import { normalizeReservationsList } from './normalizers';

export const fetchReservationsList = params => dispatch => {
  dispatch(reservationsStateChanged(api_states.IN_PROGRESS));
  return RestClient.getReservations(params).then(
    response => {
      dispatch(reservationsStateChanged(api_states.DONE));
      const normalized = normalizeReservationsList(response);
      dispatch({
        type: types.RESERVATIONS_FETCHED,
        list: normalized
      });
    },
    () => {
      dispatch(reservationsStateChanged(api_states.DONE));
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};

export const reservationsStateChanged = state => ({
  type: types.RESERVATIONS_STATE_CHANGED,
  state
});
