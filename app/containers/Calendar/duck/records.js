import { Record } from 'immutable';
import { api_states } from '../../../duck/consts';

export const Calendar = Record({
  state: api_states.DONE,
  reservations: []
});

export const Reservation = Record({
  id: null,
  title: '',
  start: null,
  end: null,
  eventColor: '#2196F3',
  equipmentName: '',
  userName: '',
  readableStart: '',
  readableEnd: '',
  description: ''
  //allDay: false
});
