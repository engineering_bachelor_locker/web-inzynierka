import types from './types';
import { Calendar } from './records';

export const calendar = (state = new Calendar(), action) => {
  switch (action.type) {
    case types.RESERVATIONS_FETCHED:
      return state.set('reservations', action.list);
    case types.RESERVATIONS_STATE_CHANGED:
      return state.set('state', action.state);
  }

  return state;
};
