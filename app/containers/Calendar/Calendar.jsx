import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import styled from 'styled-components';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';
import Drawer from '@material-ui/core/Drawer';

import 'react-big-calendar/lib/css/react-big-calendar.css';

import { PageHeader } from '../../components/Typography/Headers';
import {
  CalendarWrapper,
  ReservationSummary,
  EditReservation,
  NewReservation
} from './styled-components';
import LinearLoader from '../../components/Loaders/LinearProgress';

import { VIEWS, MIN_DAY_TIME, MAX_DAY_TIME } from './duck/consts';

import { isCalendarLoading, getReservationList } from './duck/selectors';
import { fetchReservationsList } from './duck/actions';
import { isAdmin } from '../../duck/selectors';
import { appUrls } from '../../services/appUrls';
import { NEW_FORM_ID } from '../../duck/consts';

const localizer = BigCalendar.momentLocalizer(moment);

const mapStateToProps = state => ({
  isLoading: isCalendarLoading(state),
  reservations: getReservationList(state),
  isAdmin: isAdmin(state)
});

const mapDispatchToProps = dispatch => ({
  fetchReservationsList: params => dispatch(fetchReservationsList(params))
});

class Calendar extends PureComponent {
  state = {
    isSummaryDrawerOpen: false,
    isNewDrawerOpen: false,
    selectedEvent: null,
    starts: null,
    starts_at: null,
    ends: null,
    ends_at: null
  };

  componentDidMount = () => {
    const min_date = moment()
      .startOf('month')
      .startOf('day')
      .format('YYYY-MM-DD');
    const max_date = moment()
      .add(1, 'month')
      .endOf('month')
      .format('YYYY-MM-DD');
    this.props.fetchReservationsList({ min_date, max_date });
  };

  render = () => {
    const { t, className, isLoading, reservations, isAdmin } = this.props;
    const {
      isSummaryDrawerOpen,
      selectedEvent,
      isNewDrawerOpen,
      starts,
      starts_at,
      ends,
      ends_at
    } = this.state;
    const event = selectedEvent ? selectedEvent.toJS() : {};

    return (
      <div className={className}>
        <LinearLoader isLoading={isLoading} />
        <PageHeader>{t('calendar.title')}</PageHeader>
        <CalendarWrapper>
          <BigCalendar
            selectable
            localizer={localizer}
            events={reservations}
            views={VIEWS}
            onNavigate={this.onCalendarRangeChanged}
            onSelectEvent={this.onCalendarEventClick}
            onSelectSlot={this.onCalendarSlotClick}
            min={MIN_DAY_TIME}
            max={MAX_DAY_TIME}
            drilldownView="day"
            eventPropGetter={this.calendarStyles}
          />
        </CalendarWrapper>

        <Drawer open={isSummaryDrawerOpen} onClose={this.toggleSummaryDrawer}>
          <ReservationSummary {...event} />
          {isAdmin && (
            <EditReservation url={appUrls.RESERVATION.format({ id: event.id })}>
              {t('calendar.edit_reservation')}
            </EditReservation>
          )}
        </Drawer>
        <Drawer open={isNewDrawerOpen} onClose={this.toggleNewDrawer}>
          <ReservationSummary readableStart={starts} readableEnd={ends} />
          <NewReservation
            url={`${appUrls.RESERVATION.format({
              id: NEW_FORM_ID
            })}?starts_at=${starts_at}&ends_at=${ends_at}`}
          >
            {t('calendar.add_reservation')}
          </NewReservation>
        </Drawer>
      </div>
    );
  };

  onCalendarRangeChanged = date => {
    const min_date = moment(date)
      .startOf('month')
      .startOf('day')
      .format('YYYY-MM-DD');
    const max_date = moment(date)
      .add(1, 'month')
      .endOf('month')
      .format('YYYY-MM-DD');
    this.props.fetchReservationsList({ min_date, max_date });
  };

  onCalendarEventClick = event => {
    this.setState({ selectedEvent: event }, () => {
      this.toggleSummaryDrawer();
    });
  };

  onCalendarSlotClick = event => {
    this.setState(
      {
        starts: moment(event.start).format('DD.MM.YYYY HH:mm'),
        starts_at: moment(event.start).format(),
        ends: moment(event.end).format('DD.MM.YYYY HH:mm'),
        ends_at: moment(event.end).format()
      },
      () => {
        this.toggleNewDrawer();
      }
    );
  };

  calendarStyles = event => ({
    style: {
      backgroundColor: event.eventColor,
      borderRadius: '0px',
      opacity: 0.8,
      border: '0px',
      display: 'block',
      fontSize: '14px'
    }
  });

  toggleSummaryDrawer = () =>
    this.setState({ isSummaryDrawerOpen: !this.state.isSummaryDrawerOpen });

  toggleNewDrawer = () =>
    this.setState({ isNewDrawerOpen: !this.state.isNewDrawerOpen });
}

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(Calendar);

const TranslatedComponent = translate()(ConnectedComponent);

export default styled(TranslatedComponent)`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  background-color: white;
  padding: 1.2rem 1.6rem;
  position: relative;
  ${LinearLoader} {
    position: absolute;
    top: 0;
    left: 0;
  }
`;
