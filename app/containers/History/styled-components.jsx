import styled from 'styled-components';
import React from 'react';
import HistoryIcon from '../../assets/history.png';

const Icon = styled.div`
  display: inline-block;
  width: 35px;
  height: 35px;
  background-image: url(${HistoryIcon});
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  vertical-align: middle;
`;

const RoomName = styled.span`
  display: inline-block;
  box-sizing: border-box;
  font-size: 0.8rem;
  font-weight: 600;
  margin-left: 0.6rem;
  color: rgb(51, 51, 51);
  vertical-align: middle;
`;

const DetailsWrapper = styled.div`
  display: inline-block;
  width: calc(100% - 65px);
  position: absolute;
  right: 0.6rem;
  text-align: right;
  margin-top: 5px;
`;

const UserName = styled.span`
  display: inline-block;
  color: rgb(51, 51, 51);
  font-size: 0.7rem;
  margin-right: 0.6rem;
  font-weight: 600;
`;

const TimeEntry = styled(UserName)`
  font-style: italic;
  font-weight: 400;
`;

const HistoryComponent = ({
  className,
  first_name,
  last_name,
  entry_time,
  room_name
}) => (
  <div className={className}>
    <Icon />
    <RoomName>{room_name}</RoomName>
    <DetailsWrapper>
      <UserName>
        {first_name} {last_name}
      </UserName>
      <TimeEntry>{entry_time}</TimeEntry>
    </DetailsWrapper>
  </div>
);

export const HistoryRow = styled(HistoryComponent)`
  display: inline-block;
  width: 100%;
  box-sizing: border-box;
  border: 1px dashed rgb(51, 51, 51);
  border-radius: 5px;
  margin: 0.6rem 0;
  padding: 0.4rem 0.8rem;
  transition: all 0.3s;
  position: relative;
`;

export const ScrollbarsWrapper = styled.div`
  width: 100%;
  height: 100%;
  padding: 1.2rem 1.6rem;
  box-sizing: border-box;
  display: inline-block;
  position: relative;
`;

export const HistoryWrapper = styled.div`
  display: inline-block;
  width: 100%;
`;
