import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import styled from 'styled-components';
import { Scrollbars } from 'react-custom-scrollbars';
import { grid } from '../../styles/grid';
import LinearLoader from '../../components/Loaders/LinearProgress';
import { PageHeader } from '../../components/Typography/Headers';
import {
  HistoryRow,
  HistoryWrapper,
  ScrollbarsWrapper
} from './styled-components';

import { getHistoryList, isHistoryListLoading } from './duck/selectors';
import { fetchHistoryList } from './duck/actions';

const mapStateToProps = state => ({
  history: getHistoryList(state),
  isLoading: isHistoryListLoading(state)
});

const mapDispatchToProps = dispatch => ({
  fetchHistoryList: () => dispatch(fetchHistoryList())
});

class History extends PureComponent {
  componentDidMount = () => {
    this.props.fetchHistoryList();
  };

  render = () => {
    const { t, className, history, isLoading } = this.props;
    return (
      <div className={className}>
        <LinearLoader isLoading={isLoading} />
        <Scrollbars>
          <ScrollbarsWrapper>
            <PageHeader>{t('history.title')}</PageHeader>
            <HistoryWrapper>
              {history.map((history, index) => (
                <HistoryRow key={index} {...history.toJS()} />
              ))}
            </HistoryWrapper>
          </ScrollbarsWrapper>
        </Scrollbars>
      </div>
    );
  };
}

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(History);
const TranslatedComponent = translate()(ConnectedComponent);

export default styled(TranslatedComponent)`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  background-color: white;
  position: relative;
  ${HistoryWrapper} {
    ${grid.collapse('20px')}
  }
  ${HistoryRow} {
    ${grid.breakpoints({ df: 12 }, 12, '20px')}
  }
  ${LinearLoader} {
    position: absolute;
    top: 0;
  }
`;
