import { Record } from 'immutable';
import { api_states } from '../../../duck/consts';

export const History = Record({
  id: null,
  first_name: '',
  last_name: '',
  room_name: '',
  entry_time: ''
});

export const HistoryList = Record({
  list: [],
  list_state: api_states.DONE
});
