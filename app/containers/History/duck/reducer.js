import types from './types';
import { HistoryList } from './records';

export const history = (state = new HistoryList(), action) => {
  switch (action.type) {
    case types.HISTORY_FETCHED:
      return state.set('list', action.list);
    case types.HISTORY_STATE_CHANGED:
      return state.set('list_state', action.state);
  }

  return state;
};
