import { api_states } from '../../../duck/consts';

export const getHistoryList = state => state.history.list;

export const isHistoryListLoading = state =>
  state.history.list_state === api_states.IN_PROGRESS;
