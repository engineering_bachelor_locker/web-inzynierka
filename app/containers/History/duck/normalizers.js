import moment from 'moment';
import { History } from './records';

const toHistory = history => {
  return new History({
    ...history.user,
    ...history,
    room_name: history.room.name,
    entry_time: moment(history.created_at).format('DD.MM.YY - HH:mm')
  });
};

export const normalizeHistoryList = response => {
  return response.data.map(history => toHistory(history));
};
