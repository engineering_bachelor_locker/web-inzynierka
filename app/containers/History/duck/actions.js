import { message_types, api_states } from '../../../duck/consts';
import RestClient from '../../../services/RestClient';
import { addNotification } from '../../../duck/actions';
import { normalizeHistoryList } from './normalizers';
import types from './types';

export const fetchHistoryList = () => dispatch => {
  dispatch(historyStateChanged(api_states.IN_PROGRESS));
  return RestClient.getHistory().then(
    response => {
      dispatch(historyStateChanged(api_states.DONE));
      const normalized = normalizeHistoryList(response);
      dispatch({
        type: types.HISTORY_FETCHED,
        list: normalized
      });
    },
    () => {
      dispatch(historyStateChanged(api_states.DONE));
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};

export const historyStateChanged = state => ({
  type: types.HISTORY_STATE_CHANGED,
  state
});
