import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import styled from 'styled-components';
import { Scrollbars } from 'react-custom-scrollbars';
//import { grid } from '../../styles/grid';
import LinearLoader from '../../components/Loaders/LinearProgress';
import { PageHeader } from '../../components/Typography/Headers';
import Loader from '../../components/Loaders/CircularLoader';
import {
  ScrollbarsWrapper,
  CodeWrapper,
  Code,
  Description,
  LockerImg,
  RfidButtonsWrapper,
  GeneratePIN,
  BlockAndGeneratePIN
} from './styled-components';

import Step1 from '../../assets/locker/step1.jpg';
import Step2 from '../../assets/locker/step2.jpg';
import Step3 from '../../assets/locker/step3.jpg';

import {
  fetchRfidAssigmentCode,
  generateRfidAssignmentCode,
  blockRfidCardAndGenerateCode
} from '../../duck/actions';
import {
  isRfidLoading,
  getRfidAssignmentCode,
  hasUserActiveRfidCard,
  isRfidCodeLoading
} from '../../duck/selectors';

const mapStateToProps = state => ({
  isLoading: isRfidLoading(state),
  isCodeLoading: isRfidCodeLoading(state),
  rfidCode: getRfidAssignmentCode(state),
  hasUserActiveRfidCard: hasUserActiveRfidCard(state)
});

const mapDispatchToProps = dispatch => ({
  fetchRfidAssigmentCode: () => dispatch(fetchRfidAssigmentCode()),
  generateRfidAssignmentCode: () => dispatch(generateRfidAssignmentCode()),
  blockRfidCardAndGenerateCode: () => dispatch(blockRfidCardAndGenerateCode())
});

class CardPanel extends PureComponent {
  componentDidMount = () => {
    this.props.fetchRfidAssigmentCode();
  };

  render = () => {
    const {
      t,
      className,
      isLoading,
      rfidCode,
      hasUserActiveRfidCard,
      isCodeLoading
    } = this.props;
    const showGeneratePINbutton =
      !rfidCode && !hasUserActiveRfidCard && !isCodeLoading;
    const showBlockButon = !rfidCode && hasUserActiveRfidCard && !isCodeLoading;
    return (
      <div className={className}>
        <LinearLoader isLoading={isLoading} />
        <Scrollbars>
          <ScrollbarsWrapper>
            <PageHeader>{t('rfid.title')}</PageHeader>
            <CodeWrapper>
              {!showBlockButon && t('rfid.assignment_code_title')}
              {!isLoading && (
                <RfidButtonsWrapper>
                  {rfidCode && <Code>{rfidCode}</Code>}
                  {showGeneratePINbutton && (
                    <GeneratePIN
                      onClick={this.props.generateRfidAssignmentCode}
                    >
                      {t('rfid.generate_code')}
                    </GeneratePIN>
                  )}
                  {showBlockButon && (
                    <BlockAndGeneratePIN
                      onClick={this.props.blockRfidCardAndGenerateCode}
                    >
                      {t('rfid.block_card')}
                    </BlockAndGeneratePIN>
                  )}
                  {isCodeLoading && (
                    <Loader isVisible={true} color="secondary" size={20} />
                  )}
                </RfidButtonsWrapper>
              )}
            </CodeWrapper>
            <Description
              dangerouslySetInnerHTML={{
                __html: t('rfid.assignment_description')
              }}
            />
            <LockerImg
              src={Step1}
              hint={t('rfid.step1')}
              subtitle={t('rfid.locker_img')}
            />
            <LockerImg
              src={Step2}
              hint={t('rfid.step2')}
              subtitle={t('rfid.locker_img')}
            />
            <LockerImg
              src={Step3}
              hint={t('rfid.step3')}
              subtitle={t('rfid.locker_img')}
            />
          </ScrollbarsWrapper>
        </Scrollbars>
      </div>
    );
  };
}

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(CardPanel);

const TranslatedComponent = translate()(ConnectedComponent);

export default styled(TranslatedComponent)`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  background-color: white;
  position: relative;
  ${LinearLoader} {
    position: absolute;
    top: 0;
  }
`;
