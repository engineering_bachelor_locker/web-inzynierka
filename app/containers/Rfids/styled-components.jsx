import React from 'react';
import styled from 'styled-components';

export const ScrollbarsWrapper = styled.div`
  width: 100%;
  min-height: 100%;
  padding: 1.2rem 1.6rem;
  box-sizing: border-box;
  display: inline-block;
  position: relative;
`;

export const Code = styled.span`
  display: inline-block;
  padding: 0.4rem 0.8rem;
  font-size: 1.1rem;
  color: white;
  background-color: #3d5afe;
  border-radius: 5px;
  line-height: 1.2;
  vertical-align: middle;
  letter-spacing: 4px;
  margin-left: 0.4rem;
`;

export const GeneratePIN = styled(Code)`
  letter-spacing: normal;
  text-transform: uppercase;
`;

export const BlockAndGeneratePIN = styled(Code)`
  background-color: #d81b60;
  letter-spacing: normal;
  cursor: pointer;
`;

export const RfidButtonsWrapper = styled.div`
  display: inline-block;
  min-width: 250px;
  min-height: 80px;
  text-align: center;
`;

export const CodeWrapper = styled.div`
  position: absolute;
  right: 2.4rem;
  top: 1.6rem;
  font-size: 0.8rem;
  color: rgb(51, 51, 51);
`;

export const Description = styled.div`
  display: inline-block;
  width: 100%;
  margin-top: 1.6rem;
  margin-bottom: 1.6rem;
  > p {
    margin-top: 0.6rem;
    width: 100%;
    color: rgb(51, 51, 51);
    line-height: 1.3;
    font-size: 0.85rem;
    padding-right: 2.4rem;
    box-sizing: border-box;
  }
`;

const ImgSubtitle = styled.span`
  display: inline-block;
  width: 100%;
  font-size: 0.67rem;
  color: rgb(51, 51, 51);
  opacity: 0.8;
  font-style: italic;
`;

const Hint = styled.span`
  display: inline-block;
  width: 100%;
  line-height: 1.2;
  color: rgb(51, 51, 51);
  margin-bottom: 0.3rem;
  font-size: 0.85rem;
`;

const LockerImgComponent = ({ className, src, hint, subtitle }) => (
  <div className={className}>
    <Hint>{hint}</Hint>
    <img src={src} alt="locker img" />
    <ImgSubtitle>{subtitle}</ImgSubtitle>
  </div>
);

export const LockerImg = styled(LockerImgComponent)`
  display: inline-block;
  width: 100%;
  margin-bottom: 0.8rem;
  img {
    display: inline-block;
    border: 2px solid rgb(51, 51, 51);
  }
`;
