import { appUrls } from '../../services/appUrls';

export const dashboardTabs = {
  RESERVATIONS: 'reservations',
  RESERVATION: 'reservation',
  CALENDAR: 'calendar',
  EQUIPMENT: 'equipment',
  USERS: 'users',
  USER: 'user',
  ROOMS: 'rooms',
  ROOM: 'room',
  HISTORY: 'history',
  RFID: 'rfid'
};

export const DASHBOARD_ADMIN_LIST_ITEMS = [
  {
    name: 'pages.admin_reservations',
    disabled: false,
    id: `id_${dashboardTabs.RESERVATIONS}`,
    url: appUrls.RESERVATIONS
  },
  {
    name: 'pages.calendar',
    disabled: false,
    id: `id_${dashboardTabs.CALENDAR}`,
    url: appUrls.CALENDAR
  },
  {
    name: 'pages.equipment',
    disabled: false,
    id: `id_${dashboardTabs.EQUIPMENT}`,
    url: appUrls.EQUIPMENTS
  },
  {
    name: 'pages.rooms',
    disabled: false,
    id: `id_${dashboardTabs.ROOMS}`,
    url: appUrls.ROOMS
  },
  {
    name: 'pages.history',
    disabled: false,
    id: `id_${dashboardTabs.HISTORY}`,
    url: appUrls.HISTORY_LIST
  },
  {
    name: 'pages.rfid',
    disabled: false,
    id: `id_${dashboardTabs.RFID}`,
    url: appUrls.RFID
  }
];

export const DASHBOARD_USER_LIST_ITEMS = [
  {
    name: 'pages.reservations',
    disabled: false,
    id: `id_${dashboardTabs.RESERVATIONS}`,
    url: appUrls.RESERVATIONS
  },
  {
    name: 'pages.calendar',
    disabled: false,
    id: `id_${dashboardTabs.CALENDAR}`,
    url: appUrls.CALENDAR
  },
  {
    name: 'pages.equipment',
    disabled: false,
    id: `id_${dashboardTabs.EQUIPMENT}`,
    url: appUrls.EQUIPMENTS
  },
  {
    name: 'pages.rooms',
    disabled: false,
    id: `id_${dashboardTabs.ROOMS}`,
    url: appUrls.ROOMS
  },
  {
    name: 'pages.history',
    disabled: false,
    id: `id_${dashboardTabs.HISTORY}`,
    url: appUrls.HISTORY_LIST
  },
  {
    name: 'pages.rfid',
    disabled: false,
    id: `id_${dashboardTabs.RFID}`,
    url: appUrls.RFID
  }
];
