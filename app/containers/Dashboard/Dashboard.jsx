import React, { Component } from 'react';
import styled from 'styled-components';
import { Switch, Redirect, Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { appUrls } from '../../services/appUrls';
import { Banner } from '../../components/Banner';
import BannerImg from '../../assets/banner.jpg';
import VerticalNavigation from '../../components/VerticalNavigation';
import { grid } from '../../styles/grid';

import {
  DASHBOARD_ADMIN_LIST_ITEMS,
  DASHBOARD_USER_LIST_ITEMS
} from './consts';
import { USER_ROLES } from '../../duck/consts';

import Reservations from '../Reservations/Reservations';
import ReservationForm from '../Reservations/ReservationForm';
import Calendar from '../Calendar/Calendar';
import Equipment from '../Equipment/Equipment';
import EquipmentForm from '../Equipment/EquipmentForm';
import Users from '../Users/Users';
import Rooms from '../Rooms/Rooms';
import RoomForm from '../Rooms/RoomForm';
import RoomView from '../Rooms/Room';
import History from '../History/History';
import CardPanel from '../Rfids/CardPanel';

import { signOut } from '../../duck/actions';
import { getUserRole } from '../../duck/selectors';

const mapStateToProps = state => ({
  userRole: getUserRole(state)
});

const mapDispatchToProps = () => ({
  signOut: () => signOut()
});

class Dashboard extends Component {
  render = () => {
    const { className, location } = this.props;
    const userContent = this.getContentForRole();

    return (
      <section className={className}>
        <VerticalNavigation location={location} itemList={userContent.list} />
        <Content>
          <Switch>
            <Route
              exact
              path={appUrls.RESERVATIONS}
              component={params => <Reservations {...params} />}
            />
            <Route
              exact
              path={appUrls.RESERVATION}
              component={params => <ReservationForm {...params} />}
            />
            <Route
              exact
              path={appUrls.CALENDAR}
              component={params => <Calendar {...params} />}
            />
            <Route
              exact
              path={appUrls.EQUIPMENTS}
              component={params => <Equipment {...params} />}
            />
            <Route
              exact
              path={appUrls.EQUIPMENT}
              component={params => <EquipmentForm {...params} />}
            />
            <Route
              exact
              path={appUrls.ROOM_EQUIPMENT}
              component={params => <EquipmentForm {...params} />}
            />
            <Route
              exact
              path={appUrls.USERS}
              component={params => <Users {...params} />}
            />
            <Route
              exact
              path={appUrls.ROOMS}
              component={params => <Rooms {...params} />}
            />
            <Route
              exact
              path={appUrls.ROOM}
              component={params => <RoomForm {...params} />}
            />
            <Route
              exact
              path={appUrls.ROOM_VIEW}
              component={params => <RoomView {...params} />}
            />
            <Route
              exact
              path={appUrls.HISTORY_LIST}
              component={params => <History {...params} />}
            />
            <Route
              exact
              path={appUrls.RFID}
              component={params => <CardPanel {...params} />}
            />
            <Redirect path={appUrls.DASHBOARD} to={userContent.default} />
          </Switch>
        </Content>
        <Banner backgroundImage={BannerImg} />
      </section>
    );
  };

  getContentForRole = () => {
    const { userRole } = this.props;
    switch (userRole) {
      case USER_ROLES.ADMIN:
        return {
          list: DASHBOARD_ADMIN_LIST_ITEMS,
          default: appUrls.RESERVATIONS
        };
      case USER_ROLES.CASUAL:
        return {
          list: DASHBOARD_USER_LIST_ITEMS,
          default: appUrls.RESERVATIONS
        };
      default:
        return {};
    }
  };
}

const Content = styled.div`
  height: 100%;
  position: relative;
  background-color: white;
  box-sizing: border-box;
`;

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);

export default styled(withRouter(ConnectedComponent))`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  height: 100vh;
  padding-top: 56px;
  ${VerticalNavigation} {
    box-sizing: border-box;
    ${grid.breakpoints({ df: 18, lm: 13 }, 100, '0px')};
  }
  ${Content} {
    box-sizing: border-box;
    ${grid.breakpoints({ df: 62, lm: 56 }, 100, '0px')};
  }
  ${Banner} {
    box-sizing: border-box;
    box-shadow: inset 13px 1px 18px -7px rgba(0, 0, 0, 0.16);
    ${grid.breakpoints({ df: 20, lm: 31 }, 100, '0px')};
  }
`;
