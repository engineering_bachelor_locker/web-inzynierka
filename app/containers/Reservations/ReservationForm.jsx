import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import styled from 'styled-components';
import { Scrollbars } from 'react-custom-scrollbars';

import Date from '../../components/forms/Date';
import Select from '../../components/forms/SelectInput';
import TextInput from '../../components/forms/TextInput';

import {
  InputWrapper,
  InputsRowWrapper
} from '../../components/forms/InputWrapper';
import { PageHeader } from '../../components/Typography/Headers';
import ButtonsBar, {
  ButtonsBarWrapper
} from '../../components/Buttons/ButtonsBar';

import { FormWrapper, ScrollbarsWrapper } from './styled-components';

import { appUrls } from '../../services/appUrls';
import { redirect } from '../../duck/actions';
import {
  saveReservation,
  removeReservation,
  fetchReservation,
  setFormField,
  clearFormData,
  fetchEquipmentList
} from './duck/actions';
import { NEW_FORM_ID } from '../../duck/consts';
import {
  isReservationFormLoading,
  getReservationFormData,
  getEquipmentList
} from './duck/selectors';
import { queryStringToObject } from '../../utils/history';

const mapStateToProps = state => ({
  isLoading: isReservationFormLoading(state),
  formData: getReservationFormData(state),
  equipmentList: getEquipmentList(state)
});

const mapDispatchToProps = dispatch => ({
  fetchReservation: id => dispatch(fetchReservation(id)),
  setFormField: (name, value) => dispatch(setFormField(name, value)),
  saveReservation: () => dispatch(saveReservation()),
  removeReservation: () => dispatch(removeReservation()),
  redirect: () => dispatch(redirect(appUrls.RESERVATIONS)),
  clearFormData: () => dispatch(clearFormData()),
  fetchEquipmentList: () => dispatch(fetchEquipmentList())
});
class ReservationForm extends PureComponent {
  componentDidMount = () => {
    const { id } = this.props.match.params;
    if (id && id !== NEW_FORM_ID) {
      this.props.fetchReservation(id);
    }
    this.props.fetchEquipmentList();
    const query = queryStringToObject(this.props.location.search);
    if (query.starts_at && query.ends_at) {
      this.props.setFormField('starts_at', query.starts_at);
      this.props.setFormField('ends_at', query.ends_at);
    }
  };

  componentWillUnmount = () => this.props.clearFormData();

  render = () => {
    const {
      t,
      className,
      isLoading,
      formData,
      setFormField,
      equipmentList
    } = this.props;

    const { starts_at, ends_at, equipment_id, description } = formData;

    return (
      <div className={className}>
        <FormWrapper>
          <Scrollbars>
            <ScrollbarsWrapper>
              <PageHeader>{t('reservations.form_header')}</PageHeader>
              <InputsRowWrapper>
                <InputWrapper width={6}>
                  <Date
                    id="reservation_starts_at"
                    name="starts_at"
                    value={starts_at}
                    label={t('reservations.time_from')}
                    helperText={t('reservations.time_from_hint')}
                    validate={() => true}
                    onChange={(name, value) => setFormField(name, value)}
                  />
                </InputWrapper>
                <InputWrapper width={6}>
                  <Date
                    id="reservation_ends_at"
                    name="ends_at"
                    value={ends_at}
                    label={t('reservations.time_to')}
                    helperText={t('reservations.time_to_hint')}
                    validate={() => true}
                    initialFocusedDate={starts_at}
                    onChange={(name, value) => setFormField(name, value)}
                  />
                </InputWrapper>
              </InputsRowWrapper>
              <InputsRowWrapper>
                <InputWrapper width={12}>
                  <Select
                    id="reservation_equipment_id"
                    name="equipment_id"
                    label={t('reservations.equipment')}
                    selectValues={equipmentList}
                    value={equipment_id}
                    helperText={t('reservations.equipment_hint')}
                    onChange={(name, value) => setFormField(name, value)}
                  />
                </InputWrapper>
              </InputsRowWrapper>
              <InputsRowWrapper>
                <InputWrapper width={11}>
                  <TextInput
                    label={t('reservations.reservation_description')}
                    fullWidth
                    multiline
                    rows={5}
                    type="text"
                    name="description"
                    id="room_description"
                    helperText={t('reservations.reservation_description_hint')}
                    validate={() => true}
                    pristine={true}
                    value={description}
                    onChange={(name, value) => setFormField(name, value)}
                  />
                </InputWrapper>
              </InputsRowWrapper>
            </ScrollbarsWrapper>
          </Scrollbars>
        </FormWrapper>
        <ButtonsBarWrapper>
          <ButtonsBar
            id="payment_settings_btn_bar"
            leftBtnId="go_to_dashboard_btn"
            leftBtnText={t('common.go_back')}
            leftBtnAction={this.props.redirect}
            removeBtnId="reservation_remove_button"
            removeBtnText={t('common.remove')}
            removeBtnAction={this.props.removeReservation}
            saveBtnId="reservation_save_button"
            saveBtnText={t('common.save')}
            saveBtnAction={this.props.saveReservation}
            isLoaderVisible={isLoading}
          />
        </ButtonsBarWrapper>
      </div>
    );
  };
}

const TranslatedComponent = translate()(ReservationForm);

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(TranslatedComponent);

export default styled(ConnectedComponent)`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  background-color: white;
  overflow: hidden;
  position: relative;
  ${ScrollbarsWrapper} {
    height: calc(100% - 100px);
  }
`;
