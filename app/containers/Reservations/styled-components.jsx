import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import ReservationIcon from '../../assets/microscope.png';
import { appUrls } from '../../services/appUrls';
import EditButton from '../../components/Buttons/button';
import MaterialIcon from '../../components/MaterialIcon';

const PinNumber = styled.span`
  width: 100%;
  box-sizing: border-box;
  font-size: 0.8rem;
  color: rgb(51, 51, 51);
  font-weight: 600;
  margin-right: 0.8rem;
`;

const Icon = styled.div`
  display: inline-block;
  width: 35px;
  height: 35px;
  background-image: url(${ReservationIcon});
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  vertical-align: middle;
`;

const EquipmentName = styled.span`
  display: inline-block;
  box-sizing: border-box;
  font-size: 0.8rem;
  font-weight: 600;
  margin-right: 0.6rem;
  color: rgb(51, 51, 51);
  max-width: 420px;
`;

const ReservationTime = styled.span`
  display: inline-block;
  color: rgb(51, 51, 51);
  font-size: 0.8rem;
  opacity: 0.7;
  margin-top: 4px;
`;

const ReservationDetails = styled.div`
  display: inline-block;
  vertical-align: middle;
  margin-top: 4px;
  width: calc(100% - 70px);
  position: relative;
`;

const Wrapper = styled.div`
  display: inline-block;
  box-sizing: border-box;
  min-width: 120px;
  position: absolute;
  right: 0.8rem;
`;

const ReservationComponent = ({ className, onClick, id, pin, name, time }) => (
  <Link to={appUrls.RESERVATION.format({ id })}>
    <div className={className} onClick={onClick}>
      <Icon />
      <ReservationDetails>
        {name && <EquipmentName>{name}</EquipmentName>}
        <Wrapper>
          {pin && <PinNumber>PIN: {pin}</PinNumber>}
          {time && <ReservationTime>{time}</ReservationTime>}
        </Wrapper>
      </ReservationDetails>
    </div>
  </Link>
);

export const Reservation = styled(ReservationComponent)`
  display: inline-block;
  width: 100%;
  box-sizing: border-box;
  border: 1px dashed rgb(51, 51, 51);
  border-radius: 5px;
  margin: 0.8rem 0;
  padding: 0.4rem 0.8rem;
  transition: all 0.3s;
  cursor: pointer;
  position: relative;
  ${ReservationDetails} {
    position: absolute;
    right: 0;
  }
  &:hover {
    transform: translate(-2px, -2px);
    border: 1px dashed #3d5afe;
  }
`;

const AddReservationComponent = ({ className, url, children, id }) => (
  <div className={className} id={id}>
    <Link to={url}>
      <EditButton>
        {children}
        <MaterialIcon iconName="add" />
      </EditButton>
    </Link>
  </div>
);

export const AddReservation = styled(AddReservationComponent)`
  ${MaterialIcon} {
    margin-left: 0.3rem;
  }
  position: absolute;
  right: 1.2rem;
  top: 0.4rem;
  z-index: 20;
`;

export const ReservationsWrapper = styled.div`
  display: inline-block;
  width: 100%;
  margin-top: 0.4rem;
`;

export const ScrollbarsWrapper = styled.div`
  width: 100%;
  height: 100%;
  padding: 1.2rem 1.6rem;
  box-sizing: border-box;
  display: inline-block;
  position: relative;
`;

export const FormWrapper = styled(ScrollbarsWrapper)`
  position: relative;
  padding: 0;
`;

export const ReservationFormHeader = styled.h3`
  display: inline-block;
  width: 100%;
  line-height: 1.2;
  font-size: 1.2rem;
  color: rgb(51, 51, 51);
  margin-bottom: 0.8rem;
`;

export const ButtonsWrapper = styled.div`
  display: inline-block;
  width: 100%;
  position: relative;
`;
