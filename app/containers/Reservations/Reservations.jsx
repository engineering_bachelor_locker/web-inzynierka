import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import styled from 'styled-components';
import { Scrollbars } from 'react-custom-scrollbars';

import { PageHeader } from '../../components/Typography/Headers';
import {
  Reservation,
  AddReservation,
  ScrollbarsWrapper,
  ReservationsWrapper
} from './styled-components';
import { grid } from '../../styles/grid';
import {
  getReservationsList,
  isReservationsListLoading
} from './duck/selectors';
import { fetchReservationsList } from './duck/actions';
import { NEW_FORM_ID } from '../../duck/consts';
import { appUrls } from '../../services/appUrls';

import LinearLoader from '../../components/Loaders/LinearProgress';
import { isAdmin, getUserId } from '../../duck/selectors';

const mapStateToProps = state => ({
  reservations: getReservationsList(state),
  isLoading: isReservationsListLoading(state),
  isAdmin: isAdmin(state),
  userId: getUserId(state)
});
const mapDispatchToProps = dispatch => ({
  fetchReservationsList: params => dispatch(fetchReservationsList(params))
});

class Reservations extends PureComponent {
  componentDidMount = () => {
    if (this.props.isAdmin) {
      this.props.fetchReservationsList();
    } else {
      this.props.fetchReservationsList({ author_id: this.props.userId });
    }
  };

  render = () => {
    const { t, className, isLoading, reservations } = this.props;

    return (
      <div className={className}>
        <LinearLoader isLoading={isLoading} />
        <Scrollbars>
          <ScrollbarsWrapper>
            <PageHeader>{t('reservations.title')}</PageHeader>
            <AddReservation
              id="add-reservation-button"
              url={appUrls.RESERVATION.format({ id: NEW_FORM_ID })}
            >
              {t('reservations.add_reservation')}
            </AddReservation>
            <ReservationsWrapper>
              {reservations.map(reservation => (
                <Reservation key={reservation.id} {...reservation.toJS()} />
              ))}
            </ReservationsWrapper>
          </ScrollbarsWrapper>
        </Scrollbars>
      </div>
    );
  };
}

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(Reservations);
const TranslatedComponent = translate()(ConnectedComponent);

export default styled(TranslatedComponent)`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  background-color: white;
  overflow: hidden;
  position: relative;
  ${ReservationsWrapper} {
    ${grid.collapse('20px')}
  }
  ${Reservation} {
    ${grid.breakpoints({ df: 12 }, 12, '20px')}
  }
  ${LinearLoader} {
    position: absolute;
    top: 0;
  }
  ${ScrollbarsWrapper} {
    position: relative;
    ${AddReservation} {
      position: absolute;
      right: 1.2rem;
      top: 0.4rem;
      z-index: 20;
    }
  }
`;
