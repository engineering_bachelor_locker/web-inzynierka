import { message_types, api_states } from '../../../duck/consts';
import RestClient from '../../../services/RestClient';
import { addNotification, redirect } from '../../../duck/actions';
import { appUrls } from '../../../services/appUrls';
import types from './types';
import {
  normalizeReservationsList,
  normalizeReservationData,
  normalizeEquipmentList
} from './normalizers';
import { getReservationFormData } from './selectors';

export const saveReservation = () => (dispatch, getState) => {
  const data = getReservationFormData(getState());
  dispatch(reservationFormStateChanged(api_states.IN_PROGRESS));
  return RestClient.saveReservation(data).then(
    () => {
      dispatch(reservationFormStateChanged(api_states.DONE));
      dispatch(redirect(appUrls.RESERVATIONS));
    },
    () => {
      dispatch(reservationFormStateChanged(api_states.DONE));
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};

export const removeReservation = () => (dispatch, getState) => {
  const data = getReservationFormData(getState());
  dispatch(reservationFormStateChanged(api_states.IN_PROGRESS));
  return RestClient.removeReservation(data.id).then(
    () => {
      dispatch(reservationFormStateChanged(api_states.DONE));
      dispatch(redirect(appUrls.RESERVATIONS));
    },
    () => {
      dispatch(reservationFormStateChanged(api_states.DONE));
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};

export const fetchReservationsList = params => dispatch => {
  dispatch(reservationsStateChanged(api_states.IN_PROGRESS));
  return RestClient.getReservations(params).then(
    response => {
      dispatch(reservationsStateChanged(api_states.DONE));
      const normalized = normalizeReservationsList(response);
      dispatch({
        type: types.RESERVATIONS_FETCHED,
        list: normalized
      });
    },
    () => {
      dispatch(reservationsStateChanged(api_states.DONE));
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};

export const fetchEquipmentList = () => dispatch => {
  return RestClient.getEquipments().then(
    response => {
      const normalized = normalizeEquipmentList(response);
      dispatch({
        type: types.EQUIPMENT_LIST_FETCHED,
        list: normalized
      });
    },
    () => {
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};

export const reservationsStateChanged = state => ({
  type: types.RESERVATIONS_STATE_CHANGED,
  state
});

export const fetchReservation = id => dispatch => {
  dispatch(reservationFormStateChanged(api_states.IN_PROGRESS));
  return RestClient.getReservation(id).then(
    response => {
      dispatch(reservationFormStateChanged(api_states.DONE));
      const normalized = normalizeReservationData(response);
      dispatch({
        type: types.RESERVATION_DATA_FETCHED,
        reservation_form: normalized
      });
    },
    () => {
      dispatch(reservationFormStateChanged(api_states.DONE));
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};

export const reservationFormStateChanged = state => ({
  type: types.RESERVATION_FORM_STATE_CHANGED,
  state
});

export const setFormField = (name, value) => ({
  type: types.FORM_FIELD_SET,
  name,
  value
});

export const clearFormData = () => ({
  type: types.FORM_CLEARED
});
