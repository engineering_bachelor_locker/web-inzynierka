import types from './types';
import { Reservations, Reservation } from './records';

export const reservations = (state = new Reservations(), action) => {
  switch (action.type) {
    case types.RESERVATIONS_FETCHED:
      return state.set('list', action.list);
    case types.RESERVATIONS_STATE_CHANGED:
      return state.set('list_state', action.state);
    case types.RESERVATION_FORM_STATE_CHANGED:
      return state.set('form_state', action.state);
    case types.RESERVATION_DATA_FETCHED:
      return state.setIn(['reservation_form'], action.reservation_form);
    case types.FORM_FIELD_SET:
      return state.setIn(['reservation_form', action.name], action.value);
    case types.FORM_CLEARED:
      return state.set('reservation_form', new Reservation());
  }

  return state;
};

export const reservation_equipment_list = (state = [], action) => {
  switch (action.type) {
    case types.EQUIPMENT_LIST_FETCHED:
      return action.list;
  }
  return state;
};
