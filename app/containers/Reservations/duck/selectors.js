import { api_states } from '../../../duck/consts';

export const getReservationsList = state => state.reservations.list;

export const isReservationsListLoading = state =>
  state.reservations.list_state === api_states.IN_PROGRESS;

export const isReservationFormLoading = state =>
  state.reservations.form_state === api_states.IN_PROGRESS;

export const getReservationFormData = state =>
  state.reservations.reservation_form.toJS();

export const getEquipmentList = state => state.reservation_equipment_list;
