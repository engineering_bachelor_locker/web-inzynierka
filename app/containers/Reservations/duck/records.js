import { Record } from 'immutable';
import { api_states, reservation_states } from '../../../duck/consts';

export const Reservation = Record({
  id: null,
  state: reservation_states.INACTIVE,
  description: '',
  starts_at: null,
  ends_at: null,
  equipment_id: null,
  name: '',
  time: '',
  pin: ''
});

export const Reservations = Record({
  list: [],
  reservation_form: new Reservation(),
  list_state: api_states.DONE,
  form_state: api_states.DONE
});
