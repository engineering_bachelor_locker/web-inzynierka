import moment from 'moment';
import { Reservation } from './records';

export const normalizeReservationsList = response => {
  const rooms = response.data.map(reservation => {
    const equipment = reservation.equipment;
    return new Reservation({
      ...reservation,
      name: equipment.name || '',
      time: toReservationTime(reservation)
    });
  });
  return rooms;
};

export const normalizeReservationData = response => {
  const equipment = response.data.equipment;
  return new Reservation({
    ...response.data,
    equipment_id: equipment ? equipment.id : null
  });
};

export const normalizeEquipmentList = response =>
  response.data.map(equipment => ({
    value: equipment.id,
    name: equipment.name
  }));

const toReservationTime = reservation => {
  //TODO add proper timeformating here
  // starts_at, ends_at
  return moment(reservation.starts_at).format('DD.MM.YY - HH:mm');
};
