import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import styled from 'styled-components';
import { Scrollbars } from 'react-custom-scrollbars';

import { PageHeader } from '../../components/Typography/Headers';
import {
  Room,
  AddRoom,
  ScrollbarsWrapper,
  RoomsWrapper
} from './styled-components';
import { grid } from '../../styles/grid';
import { getRoomsList, isRoomsListLoading } from './duck/selectors';
import { fetchRoomsList } from './duck/actions';
import { NEW_FORM_ID } from '../../duck/consts';
import { appUrls } from '../../services/appUrls';

import LinearLoader from '../../components/Loaders/LinearProgress';
import { isAdmin } from '../../duck/selectors';

const mapStateToProps = state => ({
  rooms: getRoomsList(state),
  isLoading: isRoomsListLoading(state),
  isAdmin: isAdmin(state)
});
const mapDispatchToProps = dispatch => ({
  fetchRoomsList: () => dispatch(fetchRoomsList())
});

class Rooms extends PureComponent {
  componentDidMount = () => {
    this.props.fetchRoomsList();
  };

  render = () => {
    const { t, className, isLoading, rooms, isAdmin } = this.props;

    return (
      <div className={className}>
        <LinearLoader isLoading={isLoading} />
        <Scrollbars>
          <ScrollbarsWrapper>
            <PageHeader>{t('rooms.title')}</PageHeader>
            {isAdmin && (
              <AddRoom url={appUrls.ROOM.format({ id: NEW_FORM_ID })}>
                {t('rooms.add_room')}
              </AddRoom>
            )}
            <RoomsWrapper>
              {rooms.map(room => (
                <Room key={room.id} {...room.toJS()} />
              ))}
            </RoomsWrapper>
          </ScrollbarsWrapper>
        </Scrollbars>
      </div>
    );
  };
}

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(Rooms);
const TranslatedComponent = translate()(ConnectedComponent);

export default styled(TranslatedComponent)`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  background-color: white;
  overflow: hidden;
  position: relative;
  ${RoomsWrapper} {
    ${grid.collapse('20px')}
  }
  ${Room} {
    ${grid.breakpoints({ df: 12 }, 12, '20px')}
  }
  ${LinearLoader} {
    position: absolute;
    top: 0;
  }
`;
