import styled from 'styled-components';
import React from 'react';
import { Link } from 'react-router-dom';
import LabIcon from '../../assets/lab.png';
import { appUrls } from '../../services/appUrls';
import MaterialIcon from '../../components/MaterialIcon';
import EditButton from '../../components/Buttons/button';
import EquipmentImg from '../../assets/equipment.png';

const Icon = styled.div`
  display: inline-block;
  width: 35px;
  height: 35px;
  background-image: url(${LabIcon});
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  vertical-align: middle;
`;

const EquipmentIcon = styled(Icon)`
  background-image: url(${EquipmentImg});
`;

const RoomName = styled.h3`
  display: inline-block;
  box-sizing: border-box;
  font-size: 0.8rem;
  margin-left: 0.4rem;
  color: rgb(51, 51, 51);
  text-align: center;
`;

const RoomComponent = ({ className, id, name, url }) => {
  const path = url || appUrls.ROOM_VIEW.format({ id });
  return (
    <Link to={path}>
      <div className={className}>
        <Icon />
        <RoomName>{name}</RoomName>
      </div>
    </Link>
  );
};

export const Room = styled(RoomComponent)`
  display: inline-block;
  width: 100%;
  box-sizing: border-box;
  border: 1px dashed rgb(51, 51, 51);
  border-radius: 5px;
  margin: 0.8rem 0;
  padding: 0.4rem 0.8rem;
  transition: all 0.3s;
  cursor: pointer;
  &:hover {
    transform: translate(-2px, -2px);
    border: 1px dashed #3d5afe;
  }
`;

const AddRoomComponent = ({ className, url, children }) => (
  <div className={className}>
    <Link to={url}>
      <EditButton>
        {children}
        <MaterialIcon iconName="add" />
      </EditButton>
    </Link>
  </div>
);

export const AddRoom = styled(AddRoomComponent)`
  ${MaterialIcon} {
    margin-left: 0.3rem;
  }
  position: absolute;
  right: 1.2rem;
  top: 0.4rem;
  z-index: 20;
`;

export const ScrollbarsWrapper = styled.div`
  width: 100%;
  height: 100%;
  padding: 1.2rem 1.6rem;
  box-sizing: border-box;
  display: inline-block;
  position: relative;
`;

export const FormWrapper = styled(ScrollbarsWrapper)`
  position: relative;
  padding: 0;
`;

export const RoomsWrapper = styled.div`
  display: inline-block;
  width: 100%;
`;

export const RoomFormHeader = styled.h3`
  display: inline-block;
  width: 100%;
  line-height: 1.2;
  font-size: 1.2rem;
  color: rgb(51, 51, 51);
  margin-bottom: 0.8rem;
`;

export const SectionTitle = styled.h2`
  line-height: 1.46;
  display: inline-block;
  color: rgb(51, 51, 51);
  width: 100%;
  margin-bottom: 0.6rem;
  font-weight: 300;
  font-size: 1.4rem;
  width: 100%;
`;

export const SectionContent = styled.div`
  font-size: 0.8rem;
  line-height: 1.2;
  color: rgb(51, 51, 51);
  font-weight: 300;
  word-break: break-word;
  white-space: pre-line;
  min-height: 3.2rem;
  width: 100%;
`;

const EquipmentName = styled.span`
  display: inline-block;
  box-sizing: border-box;
  font-size: 0.8rem;
  font-weight: 600;
  margin-left: 0.6rem;
  color: rgb(51, 51, 51);
  vertical-align: middle;
`;

const EquipmentNumber = styled(EquipmentName)`
  font-weight: 400;
  font-style: italic;
`;

const Activity = styled(EquipmentNumber)`
  position: absolute;
  right: 0.6rem;
  top: 16px;
  font-style: normal;
`;

const DetailsWrapper = styled.div`
  display: inline-block;
  width: calc(100% - 80px);
  box-sizing: border-box;
`;

const EquipmentListComponent = ({
  className,
  room_id,
  id,
  name,
  number,
  state,
  onClick,
  url,
  canEdit
}) => {
  const path = url || appUrls.ROOM_EQUIPMENT.format({ room_id, id });
  const Wrapper = canEdit ? Link : 'div';
  return (
    <Wrapper to={path}>
      <div className={className} onClick={onClick}>
        <DetailsWrapper>
          <EquipmentIcon />
          <EquipmentName>{name}</EquipmentName>
          <EquipmentNumber>{number}</EquipmentNumber>
        </DetailsWrapper>
        <Activity>{state}</Activity>
      </div>
    </Wrapper>
  );
};

export const Equipment = styled(EquipmentListComponent)`
  display: inline-block;
  width: 100%;
  box-sizing: border-box;
  border: 1px dashed rgb(51, 51, 51);
  border-radius: 5px;
  margin: 0.6rem 0;
  padding: 0.4rem 0.8rem;
  transition: all 0.3s;
  position: relative;
  ${({ canEdit }) =>
    canEdit &&
    `
    cursor: pointer;
    &:hover {
      transform: translate(-2px, -2px);
      border: 1px dashed #3d5afe;
    }
  `}
`;

export const EquipmentWrapper = styled.div`
  display: inline-block;
  width: 100%;
  position: relative;
`;
