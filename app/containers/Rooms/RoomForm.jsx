import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import styled from 'styled-components';
import { Scrollbars } from 'react-custom-scrollbars';

import TextInput from '../../components/forms/TextInput';
import Switch from '../../components/forms/Switch';

import {
  InputWrapper,
  InputsRowWrapper
} from '../../components/forms/InputWrapper';
import { PageHeader } from '../../components/Typography/Headers';
import ButtonsBar, {
  ButtonsBarWrapper
} from '../../components/Buttons/ButtonsBar';
import { FormWrapper, ScrollbarsWrapper } from './styled-components';

import { appUrls } from '../../services/appUrls';
import { redirect } from '../../duck/actions';
import { NEW_FORM_ID } from '../../duck/consts';
import {
  saveRoom,
  removeRoom,
  fetchRoom,
  setFormField,
  clearFormData
} from './duck/actions';
import { isRoomFormLoading, getRoomFormData } from './duck/selectors';
import { room_switch_states } from './duck/consts';

const mapStateToProps = state => ({
  isLoading: isRoomFormLoading(state),
  formData: getRoomFormData(state)
});

const mapDispatchToProps = dispatch => ({
  fetchRoom: id => dispatch(fetchRoom(id)),
  setFormField: (name, value) => dispatch(setFormField(name, value)),
  saveRoom: () => dispatch(saveRoom()),
  removeRoom: () => dispatch(removeRoom()),
  redirect: () => dispatch(redirect(appUrls.ROOMS)),
  clearFormData: () => dispatch(clearFormData())
});

class ReservationForm extends PureComponent {
  componentDidMount = () => {
    const { id } = this.props.match.params;
    if (id && id !== NEW_FORM_ID) {
      this.props.fetchRoom(id);
    }
  };

  componentWillUnmount = () => this.props.clearFormData();

  render = () => {
    const { t, className, isLoading, formData, setFormField } = this.props;
    const { name, number, state, description } = formData;
    return (
      <div className={className}>
        <FormWrapper>
          <Scrollbars>
            <ScrollbarsWrapper>
              <PageHeader>{t('rooms.form_header')}</PageHeader>
              <InputsRowWrapper>
                <InputWrapper width={8}>
                  <TextInput
                    label={t('rooms.room_name')}
                    fullWidth
                    type="text"
                    name="name"
                    id="room_name"
                    helperText={t('rooms.room_name_hint')}
                    validate={() => true}
                    pristine={true}
                    value={name}
                    onChange={(name, value) => setFormField(name, value)}
                  />
                </InputWrapper>
                <InputWrapper width={4}>
                  <TextInput
                    label={t('rooms.room_number')}
                    fullWidth
                    type="text"
                    name="number"
                    id="room_name"
                    helperText={t('rooms.room_number_hint')}
                    validate={() => true}
                    pristine={true}
                    value={number}
                    onChange={(name, value) => setFormField(name, value)}
                  />
                </InputWrapper>
              </InputsRowWrapper>
              <InputsRowWrapper>
                <InputWrapper width={10}>
                  <Switch
                    activeLabel={t('rooms.room_active_state')}
                    inactiveLabel={t('rooms.room_inactive_state')}
                    switchStates={room_switch_states}
                    value={state}
                    name="state"
                    onChange={(name, value) => setFormField(name, value)}
                  />
                </InputWrapper>
              </InputsRowWrapper>
              <InputsRowWrapper>
                <InputWrapper width={10}>
                  <TextInput
                    label={t('rooms.room_description')}
                    fullWidth
                    multiline
                    rows={5}
                    type="text"
                    name="description"
                    id="room_description"
                    helperText={t('rooms.room_description_hint')}
                    validate={() => true}
                    pristine={true}
                    value={description}
                    onChange={(name, value) => setFormField(name, value)}
                  />
                </InputWrapper>
              </InputsRowWrapper>
            </ScrollbarsWrapper>
          </Scrollbars>
        </FormWrapper>
        <ButtonsBarWrapper>
          <ButtonsBar
            id="payment_settings_btn_bar"
            leftBtnId="go_to_dashboard_btn"
            leftBtnText={t('common.go_back')}
            leftBtnAction={this.props.redirect}
            removeBtnId="room_remove_button"
            removeBtnText={t('common.remove')}
            removeBtnAction={this.props.removeRoom}
            saveBtnId="room_save_button"
            saveBtnText={t('common.save')}
            saveBtnAction={this.props.saveRoom}
            isLoaderVisible={isLoading}
          />
        </ButtonsBarWrapper>
      </div>
    );
  };
}

const TranslatedComponent = translate()(ReservationForm);

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(TranslatedComponent);

export default styled(ConnectedComponent)`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  background-color: white;
  overflow: hidden;
  position: relative;
  ${ScrollbarsWrapper} {
    height: calc(100% - 100px);
  }
`;
