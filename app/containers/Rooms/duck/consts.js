import { room_states } from '../../../duck/consts';

export const room_switch_states = {
  [room_states.ACTIVE]: true,
  [room_states.INACTIVE]: false,
  switch_active: room_states.ACTIVE,
  switch_inactive: room_states.INACTIVE
};
