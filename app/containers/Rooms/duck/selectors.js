import { api_states } from '../../../duck/consts';

export const getRoomsList = state => state.rooms.list;

export const isRoomsListLoading = state =>
  state.rooms.list_state === api_states.IN_PROGRESS;

export const isRoomFormLoading = state =>
  state.rooms.form_state === api_states.IN_PROGRESS;

export const getRoomFormData = state => state.rooms.room_form.toJS();
