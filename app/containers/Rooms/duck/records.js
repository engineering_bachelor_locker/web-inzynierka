import { Record } from 'immutable';
import { api_states, room_states } from '../../../duck/consts';

export const Room = Record({
  id: null,
  name: '',
  number: '',
  state: room_states.INACTIVE,
  description: '',
  supervisors: [],
  equipment: []
});

export const Rooms = Record({
  list: [],
  room_form: new Room(),
  list_state: api_states.DONE,
  form_state: api_states.DONE
});

export const Supervisor = Record({
  first_name: '',
  last_name: '',
  role: '',
  room_id: null,
  user_id: null
});

export const Equipment = Record({
  id: null,
  name: '',
  number: '',
  room_id: '',
  state: room_states.INACTIVE
});
