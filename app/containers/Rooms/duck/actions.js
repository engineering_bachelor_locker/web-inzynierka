import { message_types, api_states } from '../../../duck/consts';
import RestClient from '../../../services/RestClient';
import { addNotification, redirect } from '../../../duck/actions';
import { appUrls } from '../../../services/appUrls';
import types from './types';
import { normalizeRoomsList, normalizeRoomData } from './normalizers';
import { getRoomFormData } from './selectors';

export const saveRoom = () => (dispatch, getState) => {
  const data = getRoomFormData(getState());
  dispatch(roomFormStateChanged(api_states.IN_PROGRESS));
  return RestClient.saveRoom(data).then(
    () => {
      dispatch(roomFormStateChanged(api_states.DONE));
      dispatch(redirect(appUrls.ROOMS));
    },
    () => {
      dispatch(roomFormStateChanged(api_states.DONE));
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};

export const removeRoom = () => (dispatch, getState) => {
  const data = getRoomFormData(getState());
  dispatch(roomFormStateChanged(api_states.IN_PROGRESS));
  return RestClient.removeRoom(data.id).then(
    () => {
      dispatch(roomFormStateChanged(api_states.DONE));
      dispatch(redirect(appUrls.ROOMS));
    },
    () => {
      dispatch(roomFormStateChanged(api_states.DONE));
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};

export const fetchRoomsList = () => dispatch => {
  dispatch(roomsStateChanged(api_states.IN_PROGRESS));
  return RestClient.getRooms().then(
    response => {
      dispatch(roomsStateChanged(api_states.DONE));
      const normalized = normalizeRoomsList(response);
      dispatch({
        type: types.ROOMS_FETCHED,
        list: normalized
      });
    },
    () => {
      dispatch(roomsStateChanged(api_states.DONE));
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};

export const roomsStateChanged = state => ({
  type: types.ROOMS_STATE_CHANGED,
  state
});

export const fetchRoom = id => dispatch => {
  dispatch(roomFormStateChanged(api_states.IN_PROGRESS));
  return RestClient.getRoom(id).then(
    response => {
      dispatch(roomFormStateChanged(api_states.DONE));
      const normalized = normalizeRoomData(response);
      dispatch({
        type: types.ROOM_DATA_FETCHED,
        room_form: normalized
      });
    },
    () => {
      dispatch(roomFormStateChanged(api_states.DONE));
      dispatch(
        addNotification({
          content: 'notifications.error',
          type: message_types.error
        })
      );
    }
  );
};

export const roomFormStateChanged = state => ({
  type: types.ROOM_FORM_STATE_CHANGED,
  state
});

export const setFormField = (name, value) => ({
  type: types.FORM_FIELD_SET,
  name,
  value
});

export const clearFormData = () => ({
  type: types.FORM_CLEARED
});
