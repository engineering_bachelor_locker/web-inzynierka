import types from './types';
import { Rooms, Room } from './records';

export const rooms = (state = new Rooms(), action) => {
  switch (action.type) {
    case types.ROOMS_FETCHED:
      return state.set('list', action.list);
    case types.ROOMS_STATE_CHANGED:
      return state.set('list_state', action.state);
    case types.ROOM_FORM_STATE_CHANGED:
      return state.set('form_state', action.state);
    case types.ROOM_DATA_FETCHED:
      return state.setIn(['room_form'], action.room_form);
    case types.FORM_FIELD_SET:
      return state.setIn(['room_form', action.name], action.value);
    case types.FORM_CLEARED:
      return state.set('room_form', new Room());
  }

  return state;
};
