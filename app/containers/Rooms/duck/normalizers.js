import { Room, Supervisor, Equipment } from './records';

export const normalizeRoomsList = response => {
  const rooms = response.data.map(room => {
    room.equipment = room.equipment || [];
    return new Room({
      ...room,
      supervisors: room.supervisors.map(
        supervisor => new Supervisor({ ...supervisor })
      )
    });
  });
  return rooms;
};

const toEquipment = room => {
  room.equipment = room.equipment || [];
  return room.equipment.map(equipment => new Equipment({ ...equipment }));
};

export const normalizeRoomData = response => {
  return new Room({
    ...response.data,
    equipment: toEquipment({ ...response.data })
  });
};
