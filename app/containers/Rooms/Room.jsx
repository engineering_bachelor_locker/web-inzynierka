import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import styled from 'styled-components';
import { Scrollbars } from 'react-custom-scrollbars';

import { PageHeader, PageSubHeader } from '../../components/Typography/Headers';
import StyledIcon from '../../components/MaterialIcon';
import ButtonsBar, {
  ButtonsBarWrapper
} from '../../components/Buttons/ButtonsBar';
import {
  FormWrapper,
  ScrollbarsWrapper,
  SectionTitle,
  SectionContent,
  Equipment,
  EquipmentWrapper
} from './styled-components';
import { EditButton } from '../../components/Buttons/button';
import { appUrls } from '../../services/appUrls';
import { redirect } from '../../duck/actions';
import {
  NEW_FORM_ID,
  getRoomStateName,
  getEquipmentStateName
} from '../../duck/consts';
import {
  saveRoom,
  removeRoom,
  fetchRoom,
  setFormField,
  clearFormData
} from './duck/actions';
import { isRoomFormLoading, getRoomFormData } from './duck/selectors';
import { isAdmin } from '../../duck/selectors';

const mapStateToProps = state => ({
  isLoading: isRoomFormLoading(state),
  formData: getRoomFormData(state),
  isAdmin: isAdmin(state)
});

const mapDispatchToProps = dispatch => ({
  fetchRoom: id => dispatch(fetchRoom(id)),
  setFormField: (name, value) => dispatch(setFormField(name, value)),
  saveRoom: () => dispatch(saveRoom()),
  removeRoom: () => dispatch(removeRoom()),
  redirect: id => dispatch(redirect(appUrls.ROOM.format({ id }))),
  equipment_redirect: (id, equipment_id) =>
    dispatch(
      redirect(appUrls.ROOM_EQUIPMENT.format({ room_id: id, id: equipment_id }))
    ),
  clearFormData: () => dispatch(clearFormData())
});

class RoomView extends PureComponent {
  componentDidMount = () => {
    const { id } = this.props.match.params;
    if (id && id !== NEW_FORM_ID) {
      this.props.fetchRoom(id);
    }
  };

  componentWillUnmount = () => this.props.clearFormData();

  render = () => {
    const { t, className, isLoading, formData, isAdmin } = this.props;
    const { id, name, number, state, description, equipment } = formData;
    return (
      <div className={className}>
        <FormWrapper>
          {isAdmin && (
            <EditButton onClick={() => this.props.redirect(id)}>
              {t('rooms.edit_room')}
            </EditButton>
          )}
          <Scrollbars>
            <ScrollbarsWrapper>
              <PageHeader>{name}</PageHeader>
              <PageSubHeader>
                {t('rooms.room_number')}
                <b>{number}</b>
                {t('rooms.room_activity')}
                <b>{t(getRoomStateName(state))}</b>
              </PageSubHeader>
              {description && [
                <SectionTitle>{t('rooms.view_description')}</SectionTitle>,
                <SectionContent>{description}</SectionContent>
              ]}
              <EquipmentWrapper>
                {isAdmin && (
                  <NewEquipmentButton
                    onClick={() =>
                      this.props.equipment_redirect(id, NEW_FORM_ID)
                    }
                  >
                    <StyledIcon iconName="add" />
                  </NewEquipmentButton>
                )}
                {equipment.length > 0 && (
                  <SectionTitle>{t('rooms.equipment_list')}</SectionTitle>
                )}
                {equipment.map((e, index) => (
                  <Equipment
                    {...e.toJS()}
                    room_id={e.room_id}
                    index={index}
                    state={t(getEquipmentStateName(e.state))}
                    canEdit={isAdmin}
                  />
                ))}
              </EquipmentWrapper>
            </ScrollbarsWrapper>
          </Scrollbars>
        </FormWrapper>
        <ButtonsBarWrapper>
          <ButtonsBar
            id="payment_settings_btn_bar"
            leftBtnId="go_to_dashboard_btn"
            leftBtnText={t('common.go_back')}
            leftBtnAction={this.props.redirect}
            isLoaderVisible={isLoading}
          />
        </ButtonsBarWrapper>
      </div>
    );
  };
}

const TranslatedComponent = translate()(RoomView);

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(TranslatedComponent);

const NewEquipmentButton = styled(EditButton)``;

export default styled(ConnectedComponent)`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  background-color: white;
  overflow: hidden;
  position: relative;
  ${ScrollbarsWrapper} {
    height: calc(100% - 100px);
  }
  ${EditButton} {
    position: absolute;
    right: 1.2rem;
    top: 1.6rem;
    z-index: 20;
  }
  ${NewEquipmentButton} {
    position: absolute;
    right: 0;
    z-index: 20;
  }
  ${SectionTitle} {
    margin-top: 1.2rem;
  }
`;
