const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');

const apiUrl = process.env.API_URL
  ? process.env.API_URL
  : 'https://locker-api.herokuapp.com';

const PATHS = {
  build: path.join(__dirname, '..', 'dist'),
  translations: path.join(__dirname, '..', 'app', 'translations')
};

module.exports = (env, options) => ({
  entry: {
    'main-index': './app/bundles/main-index',
    'dashboard/dashboard-index': './app/bundles/dashboard-index'
  },
  output: {
    path: PATHS.build,
    publicPath: '/',
    filename: '[name]-[hash].js'
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  devServer: {
    headers: { 'Access-Control-Allow-Origin': '*' },
    contentBase: './dist',
    publicPath: `http://localhost:3000/`,
    port: 3000,
    historyApiFallback: {
      rewrites: [
        {
          from: /^\/$/,
          to: '/index.html'
        },
        {
          from: /^\/dashboard\/*/,
          to: '/dashboard/index.html'
        }
      ]
    }
  },
  module: {
    rules: [
      {
        test: /\.js|.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.json$/,
        use: {
          loader: 'json-loader'
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          'file-loader',
          {
            loader: 'image-webpack-loader',
            options: {
              bypassOnDebug: true
            }
          }
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot|otf)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      API_URL: JSON.stringify(apiUrl)
    }),
    new CleanWebpackPlugin('dist', {}),
    new HtmlWebpackPlugin({
      title: 'landing-page',
      template: 'app/index.html',
      chunks: ['main-index'],
      filename: 'index.html',
      buildTimestamp: Date.now(),
      inject: true,
      hash: true
    }),
    new HtmlWebpackPlugin({
      title: 'dashboard-page',
      template: 'app/index.html',
      chunks: ['dashboard/dashboard-index'],
      filename: 'dashboard/index.html',
      buildTimestamp: Date.now(),
      inject: true,
      hash: true
    }),
    new WebpackMd5Hash(),
    new CopyWebpackPlugin([
      {
        from: PATHS.translations,
        to: path.join(PATHS.build)
      }
    ]),
    options.mode === 'production'
      ? new CompressionPlugin({
          filename: '[path].gz[query]',
          algorithm: 'gzip',
          test: /\.(js|svg|png|json)$/,
          threshold: 1024,
          minRatio: 0.8
        })
      : false,
    new ManifestPlugin({
      publicPath: '/'
    })
  ].filter(Boolean)
});
