# Laboratory equipment reservation system - web client

## Run

```
yarn start
```

## Run e2e tests

Run all tests

```
yarn test:e2e
```

Run single test by name

```
yarn test:e2e --grep 'Login'
```

vs code - remove decorators warning:

File -> Preferences -> settings ->

```
"javascript.implicitProjectConfig.experimentalDecorators": true
```
